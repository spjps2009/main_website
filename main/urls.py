from django.conf.urls import include, url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
import settings

admin.autodiscover()

urlpatterns = [
    # Examples:
    # url(r'^blog/', include('blog.urls')),
    url(r'^', include('art.urls', namespace="art", app_name="art")),
    url(r'^art/', include('art.urls', namespace="art", app_name="art")),
    url(r'^music/', include('music.urls', namespace="music", app_name="music")),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    #url(r'^art/', include('art.urls')),
]
if settings.DEBUG == True:
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]
#urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
