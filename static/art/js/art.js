$(document).foundation({
	accordion: {
		callback : function (accordion) {
			console.log(accordion);
		}
	}
});
$(function () {

    $(document).foundation();

});
$( document ).ready(function() {
    <!--//getScore("bob");-->
});

function hideArtist(link, name, id, num){
	$('#name'+id).html('<a href="#" id="name' + id + '" data-reveal-id="artistModal" onclick="displayData(\'' + name + '\')">'+ name + '</a>');
	$('.namealt' + id).remove();
	$("#hide" + id).hide();
	$("#show" + id).show();

}

function getArtist(link, name, id, num){
	$.ajax({
		 url: '/art/home/getArtist/',
		 type: 'get', //this is the default though, you don't actually need to always mention it
		 data:{"url": encodeURI(link), "name": encodeURI(name)},
		 success: function(data) {
				console.log(data);
				var artistlist = $.parseJSON(data);
				$('#name'+id).after('<a href="#" id="hide' + id + '" onclick="hideArtist(\'' + link + '\', \'' + name + '\', ' + id + ', ' + num + ');return false;"><span class="round secondary label fi-minus lead"></span></a>');
				$.each(artistlist, function(index, value){
					console.log(value['fields']['name']);
					$('#name'+id).after('<a href="#" class="namealt' + id + '" data-reveal-id="artistModal" onclick="displayData(\'' + value['fields']['name'] + '\')">' + ", " + value['fields']['name'] + '</a>');
					$("#show" + id).hide();
				});
		 },
		 failure: function(data) {
			  alert('got an error dude');
		 }
	});

}

function removeLink(link){
	$.ajax({
		 url: '/art/home/removeLink/',
		 type: 'get', //this is the default though, you don't actually need to always mention it
		 data:{"url": encodeURI(link)},
		 success: function(data) {
				console.log(data);
				location.reload();
			  	//$('#artistscore').text("score: " + data);

				//$('html, body').animate({scrolltop:$('#url' + panel).position().top}, 'fast')
		 },
		 failure: function(data) {
			  alert('got an error dude');
		 }
	});
}
function removeLinkArtist(link, name){
	$.ajax({
		 url: '/art/home/removeLinkArtist/',
		 type: 'get', //this is the default though, you don't actually need to always mention it
		 data:{"url": encodeURI(link), 'name':encodeURI(name)},
		 success: function(data) {
				console.log(data);
				location.reload();
			  	//$('#artistscore').text("score: " + data);

				//$('html, body').animate({scrolltop:$('#url' + panel).position().top}, 'fast')
		 },
		 failure: function(data) {
			  alert('got an error dude');
		 }
	});
}
function restoreLink(link){
	$.ajax({
		 url: '/art/home/restoreLink/',
		 type: 'get', //this is the default though, you don't actually need to always mention it
		 data:{"url": encodeURI(link)},
		 success: function(data) {
				location.reload();
				console.log(data);
			  	//$('#artistscore').text("score: " + data);

				//$('html, body').animate({scrolltop:$('#url' + panel).position().top}, 'fast')
		 },
		 failure: function(data) {
			  alert('got an error dude');
		 }
	});
}

function restoreLinkArtist(link, name){
	$.ajax({
		 url: '/art/home/restoreLinkArtist/',
		 type: 'get', //this is the default though, you don't actually need to always mention it
		 data:{"url": encodeURI(link), 'name':encodeURI(name)},
		 success: function(data) {
				console.log(data);
				location.reload();
			  	//$('#artistscore').text("score: " + data);

				//$('html, body').animate({scrolltop:$('#url' + panel).position().top}, 'fast')
		 },
		 failure: function(data) {
			  alert('got an error dude');
		 }
	});
}

function displayData(name){
	$('#artistName').text(name);
	$('#artistScore').text("Score: ");
	$('#artistDomain').text("Domains: ");
	$('#artistLink').text("");
	$('#artistTrash').text("");
	getScore(name);
	getDomain(name);
	getLink(name);
	getTrash(name);
}
function getScore(name){
	$.ajax({
		 url: '/art/home/getScore/' + encodeURI(name),
		 type: 'get', //this is the default though, you don't actually need to always mention it
		 success: function(data) {
			  	$('#artistScore').text("Score: " + data);
				//$('html, body').animate({scrolltop:$('#url' + panel).position().top}, 'fast')
		 },
		 failure: function(data) {
			  alert('got an error dude');
		 }
	});
}

function showMore(link, id){
	$.ajax({
		 url: '/art/home/getMore/',
		 type: 'get', //this is the default though, you don't actually need to always mention it
			data:{"url": encodeURI(link)},
		 success: function(data) {
				$("#more" + id).hide();
				var artistlist = $.parseJSON(data);
				$.each(artistlist, function(index, value){
					$('#more'+id).after("<span style='color:grey'>" + value + ", </span>");
				});
				$('#more'+id).after("<span style='color:grey; margin-top:5px'>" + "Musicians: "+ "</span>");
			 console.log(data);
		 },
		 failure: function(data) {
			  alert('got an error dude');
		 }
	});
}

function getDomain(name){
	$.ajax({
		 url: '/art/home/getDomain/' + encodeURI(name),
		 type: 'get', //this is the default though, you don't actually need to always mention it
		 success: function(data) {
				var domainlist = $.parseJSON(data);
				var domainstring = "";
				$.each(domainlist, function(index, value){
						domainstring = domainstring + value['domain'] + ", ";
				});
				domainstring = domainstring.substring(0, domainstring.length - 2);
			  $('#artistDomain').text("Domains: " + domainstring);


		 },
		 failure: function(data) {
			  alert('Got an error dude');
		 }
	});
}

function getLink(name){
	$('#artistLink').text('');
	$.ajax({
		 url: '/art/home/getLink/' + encodeURI(name),
		 type: 'get', //this is the default though, you don't actually need to always mention it
		 success: function(data) {
				var linklist = $.parseJSON(data);
				//console.log(linklist);
				//var domainstring = "";
				$.each(linklist, function(index, value){
						curr = value['fields'];
						$('#artistLink').append('<a href="#"><img src="/static/art/img/redx.png" onclick="removeLink(\'' + curr['link'] + '\');return false;" style="width:15px;height:15px;"></img></a><a href="#"><img src="/static/art/img/x.png" onclick="removeLinkArtist(\'' + curr['link'] + '\',\'' + name + '\');return false;" style="width:15px;height:15px;"></img></a>' + curr['date'].substring(5, 10) + "-" + curr['date'].substring(0, 4) + ' - <a href="' + curr['link'] + '">' + curr['link'] + '</a>' + '<br>');
				});
				//domainstring = domainstring.substring(0, domainstring.length - 2);
			  //$('#link'+ panel).text(domainstring);
		 },
		 failure: function(data) {
			  alert('Got an error dude');
		 }
	});
}

function getTrash(name){
	$('#artistTrash').text('');
	$.ajax({
		 url: '/art/home/getTrash/' + encodeURI(name),
		 type: 'get', //this is the default though, you don't actually need to always mention it
		 success: function(data) {
				var linklist = $.parseJSON(data);
				console.log(linklist);
				//var domainstring = "";
				$.each(linklist, function(index, value){
						curr = value['fields'];
						$('#artistTrash').append('<a href="#"><img src="/static/art/img/redx.png" onclick="restoreLink(\'' + curr['link'] + '\');return false;" style="width:15px;height:15px;"></img></a><a href="#"><img src="/static/art/img/x.png" onclick="restoreLinkArtist(\'' + curr['link'] + '\',\'' + name + '\');return false;" style="width:15px;height:15px;"></img></a>' + curr['date'].substring(5, 10) + "-" + curr['date'].substring(0, 4) + ' - <a href="' + curr['link'] + '">' + curr['link'] + '</a>' + '<br>');
				});
				//domainstring = domainstring.substring(0, domainstring.length - 2);
			  //$('#link'+ panel).text(domainstring);
		 },
		 failure: function(data) {
			  alert('Got an error dude');
		 }
	});
}

