from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector
from get_names.items import GetNamesItem
import urlparse
from scrapy.http.request import Request
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
import nltk
from nltk.corpus import wordnet
import lxml
from lxml import etree
import re
import enchant

class ScrapyDemoSpider(CrawlSpider):
    name = "get_names"
    allowed_domains = [
        "davidzwirner.com",
        "gagosian.com",
        "luhringaugustine.com",
        #"flcart.org",
        #"skny.com",
        #"jackshainman.com",
        #"cheimread.com",
        #"onstellarrays.com",
        #"storefrontnews.org",
        #"soho20gallery.com",
        #"303gallery.com",
        #"511gallery.com",
        #"aspacegallery.com",
        #"amrichardfineart.com",
        #"acagalleries.com",
        #"acquavellagalleries.com",
        #"adambaumgoldgallery.com",
        #"adelsongalleries.com",
        #"aboutglamour.strikingly.com",
        #"agora-gallery.com",
        #"aicongallery.com",
        #"akiraikedagallery.com/ny.htm",
        #"klotzgallery.com"
        ]
    start_urls = [
        "http://www.davidzwirner.com/",
        "http://www.gagosian.com",
        "http://www.luhringaugustine.com",
        #"http://www.flcart.org/",
        #"http://www.skny.com/",
        #"http://www.jackshainman.com/",
        #"http://www.cheimread.com/",
        #"http://onstellarrays.com/",
        #"http://storefrontnews.org/",
        #"http://soho20gallery.com/",
        #"http://303gallery.com/",
        #"http://www.511gallery.com/",
        #"http://aspacegallery.com/",
        #"http://www.amrichardfineart.com/",
        #"http://acagalleries.com/",
        #"http://www.acquavellagalleries.com/",
        #"http://adambaumgoldgallery.com/",
        #"http://adelsongalleries.com/",
        #"http://aboutglamour.strikingly.com/",
        #"http://www.agora-gallery.com/",
        #"http://www.aicongallery.com/",
        #"http://akiraikedagallery.com/ny.htm",
        #"http://www.klotzgallery.com/"
        ]
    rules = (Rule (SgmlLinkExtractor(allow="/[a-zA-Z0-9]*artist[a-zA-Z0-9]*(\/)?$"), callback="parse_items", follow= True),)


    def parse_items(self,response):
        if "artist" in str(response.url):
            print response.url
            terms = re.findall("(?<=>)[a-zA-Z0-9]+ {1}[a-zA-Z0-9]+(?=<)", response.body)
            #print terms
            names = run_xpath(terms, response)
        else:
            pass
            #names = extract_entities(nltk.clean_html(response.body))
        #print names
d = enchant.Dict("en_US")
def run_xpath(terms, html):
    if len(terms) <= 10:
        return
    #print terms
    #for term in terms:
        #term_arr = term.split(' ')
        #if d.check(term_arr[0]) and d.check(term_arr[1]):
            #print "removing term: " + term
            #terms.remove(term)
    #print terms
    hxs = HtmlXPathSelector(html)
    print "HTML Length: " + str(len(hxs.select("/*").extract()[0]))
    xpaths = []
    done = []
    for term in terms:
        xpaths.append("//*[contains(text(), '" + term + "')]")




    heads = []
    doc_lens = []
    counts = []

    for i in range(0, len(xpaths) - 1):
        for j in range(0, len(xpaths) - 1):
            if xpaths[i] != xpaths[j]:
                temp = hxs.select(xpaths[i] + "/ancestor::*[count(. | " + xpaths[j] + "/ancestor::*) = count(" + xpaths[j] + "/ancestor::*)][1]").extract()[0]
                #reject too short and reject full page html
                if str(len(hxs.select("/*").extract()[0])) == len(temp):
                    continue
                    #print xpaths[i] + " " + xpaths[j]
                if temp not in heads:
                    heads.append(temp)
                    doc_lens.append(len(temp))

    for head in heads:
        count = 0
        for term in terms:
            if term in head:
                count += 1
        counts.append(count)
        print "Length: " + str(len(head)) + ", Count: " + str(count)



    for h, l, c in zip(heads, doc_lens, counts):
        print '-'*20
        print "Length: " + str(l) + ", Count: " + str(c)
        print h
        print '-'*20

    #print zip(heads, doc_lens, counts)
    heads_info = sorted(zip(heads, doc_lens, counts), key=lambda tup: tup[2])

    good = []
    bad = []
    for term in terms:
        if "art " in term.lower() or "artist" in term.lower():
            bad.append(term)
    curr_count = 0
    for h in heads_info:
        if h[2] <= 3:
            for term in terms:
                if term in h[0]:
                    if term not in bad:
                        #print "Bad"
                        #print bad
                        #print "Good"
                        #print good
                        #print "Too less terms in head: " + term
                        bad.append(term)
            curr_count = h[2]
            continue

        new_count = 0
        for term in terms:
            if term not in bad and term not in good and term in h[0]:
                new_count+=1
        #diff_count = h[2] - curr_count
        if new_count <= 2:
            for term in terms:
                if term in h[0]:
                    if term not in bad:
                        if term not in good:
                            #print "Bad"
                            #print bad
                            #print "Good"
                            #print good
                            #print "Head has too little new terms: " + term + " " + str(h[2])
                            bad.append(term)
            curr_count = h[2]
            continue

        for term in terms:
            if term in h[0]:
                if term not in good:
                    good.append(term)


    print "Final List"
    print '-' * 20
    print "BAD TERMS"
    print '-' * 20
    print bad
    print '-' * 20
    print "GOOD TERMS"
    print '-' * 20
    print good
    print '-' * 20
    #print xpaths
    #print temp

#    while len(xpaths) != 0:
        ##print xpaths
        #count = 0
        #temp_done = []
        #temp = ""
        ##try:
        #temp = hxs.select(xpaths[0] + "/ancestor::*[count(. | " + xpaths[1] + "/ancestor::*) = count(" + xpaths[1] + "/ancestor::*)][1]").extract()[0]
                ##except:
            ##pass
            ##print xpaths
        ##print len(temp)
        ##print len(html)
        ##print terms
        #for term in terms:
            ##print term
            ##print temp
            #if term in temp:
                ##print "found"
                #count += 1
                #temp_done.append(term)
            ##else:
                ##print "notfound"
        #if count >=8 and html!=temp:
            #done.extend(temp_done)
        #for temp in temp_done:
            #for xpath in xpaths:
                ##print term + " " + xpath
                #if temp in xpath:
                    #xpaths.remove(xpath)
        #print temp_done

    #print "Final List"
    #print '-' * 20
    #print done
    #print '-' * 20
    #print xpaths
    #print temp

    #html = html.encode('utf-8').strip()
    #hparser = etree.HTMLParser(encoding="utf-8")
    #doc = lxml.html.parse(html, hparser).getroot()
    #doc = etree.parse(html, parser=hparser)
    #comment_array = hxs.select('//a/@href').extract()


def extract_entities1(text):
    tokens = nltk.tokenize.word_tokenize(text)
    pos = nltk.pos_tag(tokens)
    sentt = nltk.ne_chunk(pos, binary = False)
    person_list = []
    person = []
    name = ""
    for subtree in sentt.subtrees(filter=lambda t: t.node == 'PERSON'):
        for leaf in subtree.leaves():
            person.append(leaf[0])
        if len(person) > 1: #avoid grabbing lone surnames
            for part in person:
                name += part + ' '
            if name[:-1] not in person_list:
                person_list.append(name[:-1])
            name = ''
        person = []

    return (person_list)
FILTER = ["art", "gallery", "street", "center", "subscribe", "member", "pay", "involve", "architechture", "book", "email", "nation", "project", "now", "museum"]
def extract_entities(text):
    for sent in nltk.sent_tokenize(text):
        for chunk in nltk.ne_chunk(nltk.pos_tag(nltk.word_tokenize(sent))):
            if hasattr(chunk, 'node'):
                if chunk.node == "PERSON":
                    name = ' '.join(c[0] for c in chunk.leaves())
                    name_arr = name.split(" ")
                    fail = False
                    for f in FILTER:
                        for n in name_arr:
                            if f in n.lower():
                                fail = True
                    if len(name_arr) >= 2 and len(name_arr) < 4 and fail == False:
                        print chunk.node, ' '.join(c[0] for c in chunk.leaves())
