# -*- coding: utf-8 -*-

# Scrapy settings for get_names project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'get_names'

SPIDER_MODULES = ['get_names.spiders']
NEWSPIDER_MODULE = 'get_names.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'get_names (+http://www.yourdomain.com)'
DOWNLOADER_MIDDLEWARES = {
    'scrapy.contrib.downloadermiddleware.useragent.UserAgentMiddleware': None,
    'scrapy_fake_useragent.middleware.RandomUserAgentMiddleware': 400,
}
LOG_LEVEL="INFO"
DEPTH_LIMIT=2
concurrent_requests = 1
