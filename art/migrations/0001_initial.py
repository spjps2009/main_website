# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='RssArtistIndex',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('link', models.CharField(max_length=200)),
                ('domain', models.CharField(max_length=100)),
                ('date', models.DateTimeField()),
                ('title', models.CharField(max_length=500)),
                ('artfacts_rank', models.IntegerField()),
                ('blacklist', models.TextField()),
            ],
            options={
                'db_table': 'rss_artist_index',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='RssContent',
            fields=[
                ('link', models.CharField(max_length=200, serialize=False, primary_key=True)),
                ('domain', models.CharField(max_length=200)),
                ('date', models.DateTimeField()),
                ('title', models.CharField(max_length=500)),
                ('content', models.TextField()),
                ('blacklist', models.TextField()),
            ],
            options={
                'db_table': 'rss_content',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='SvmArt',
            fields=[
                ('link', models.CharField(max_length=200, serialize=False, primary_key=True)),
                ('prediction', models.CharField(max_length=200)),
            ],
            options={
                'db_table': 'svm_art',
                'managed': False,
            },
        ),
    ]
