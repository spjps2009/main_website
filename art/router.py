from django.conf import settings

class ArtRouter(object):
    """
    A router to control all database operations on models in the
    art application.
    """
    def db_for_read(self, model, **hints):
        """
        Attempts to read art models go to data.
        """
        if model._meta.app_label == 'art':
            return 'data'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write art models go to data.
        """
        if model._meta.app_label == 'art':
            return 'data'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the art app is involved.
        """
        if obj1._meta.app_label == 'art' or \
           obj2._meta.app_label == 'art':
           return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the art app only appears in the 'data'
        database.
        """
        if db == 'data':
            return app_label == 'art'
        elif app_label == 'art':
            return False
        return None

