from django.shortcuts import render
from django.http import HttpResponse
from art.models import ArtistScore, RssArtistIndex, RssContent, Toplists
from django.core import serializers
from django.db import connections
from django.views.decorators.csrf import csrf_exempt
import urllib2
import json
import datetime as dt
import logging
import numpy as np
#from optparse import OptionParser
import sys
from time import time
import MySQLdb
from MySQLdb import cursors
import os.path
import itertools
import operator
#from operator import itemgetter
import random
import re
from bs4 import BeautifulSoup
#from bs4 import SoupStrainer
from urlparse import urlparse
from scrapy.selector import HtmlXPathSelector
from urlparse import urljoin
from selenium import webdriver
from time import sleep
#import subprocess
import os
#import string
#from pyvirtualdisplay import Display
import pyvirtualdisplay
#from selenium.webdriver.firefox.webdriver import FirefoxBinary
from googleplaces import GooglePlaces, types
from factual import Factual
from factual.utils import circle
from collections import OrderedDict
from googlesearch import GoogleSearch
import time
from time import mktime
import math
import decimal
from random import randrange
from datetime import timedelta, time, tzinfo, datetime
import time
import tldextract
from lxml.html.clean import Cleaner
from goose import Goose
from readability.readability import Document
from boilerpipe.extract import Extractor
from cookielib import CookieJar
import daily_update as du




from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import TfidfVectorizer
#from sklearn.feature_extraction.text import HashingVectorizer
#from sklearn.feature_selection import SelectKBest, chi2
#from sklearn.linear_model import RidgeClassifier
#from sklearn.svm import LinearSVC
from sklearn.linear_model import SGDClassifier
#from sklearn.linear_model import Perceptron
#from sklearn.linear_model import PassiveAggressiveClassifier
#from sklearn.naive_bayes import BernoulliNB, MultinomialNB
#from sklearn.neighbors import KNeighborsClassifier
#from sklearn.neighbors import NearestCentroid
#from sklearn.utils.extmath import density
from sklearn.utils import check_random_state
#from sklearn import metrics
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.externals import joblib

ua = [
            "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6",
            "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:5.0) Gecko/20100101 Firefox/5.0",
            "Mozilla/5.0 (Windows NT 6.1.1; rv:5.0) Gecko/20100101 Firefox/5.0",
            "Mozilla/5.0 (X11; U; Linux i586; de; rv:5.0) Gecko/20100101 Firefox/5.0",
            "Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.1 (KHTML, like Gecko) Ubuntu/11.04 Chromium/14.0.825.0 Chrome/14.0.825.0 Safari/535.1",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/14.0.824.0 Safari/535.1",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:5.0) Gecko/20100101 Firefox/5.0",
            "Mozilla/5.0 (Macintosh; PPC MacOS X; rv:5.0) Gecko/20110615 Firefox/5.0",
            "Mozilla/5.0 (Windows; U; MSIE 9.0; WIndows NT 9.0; en-US))",
            "Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 5.2; Trident/4.0; Media Center PC 4.0; SLCC1; .NET CLR 3.0.04320)",
            "Mozilla/5.0 (Windows; U; MSIE 7.0; Windows NT 6.0; en-US)",
            "Mozilla/5.0 (compatible; Konqueror/4.5; FreeBSD) KHTML/4.5.4 (like Gecko)",
            "Opera/9.80 (Windows NT 6.1; U; es-ES) Presto/2.9.181 Version/12.00",
            "Opera/9.80 (X11; Linux x86_64; U; fr) Presto/2.9.168 Version/11.50",
            "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_8; de-at) AppleWebKit/533.21.1 (KHTML, like Gecko) Version/5.0.5 Safari/533.21.1",
            "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_7; da-dk) AppleWebKit/533.21.1 (KHTML, like Gecko) Version/5.0.5 Safari/533.21.1",
    ]

class DateTimeEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, dt.datetime):
            return int(mktime(obj.timetuple()))

        return json.JSONEncoder.default(self, obj)

class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return float(o)

        return super(DecimalEncoder, self).default(o)


def index(request):
    #template = loader.get_template('art/index.html')
    nextoffset = 0
    previousoffset = 0
    try:
        nextoffset = int(request.GET['offset'])
    except Exception:
        nextoffset = 0


    print "starting link_list"
    #link_list = RssArtistIndex.objects.raw("SELECT id, a.name, a.link, a.domain, a.date, a.title, count(*) as numArtist FROM rss_artist_index a where a.blacklist = 0 group by a.link order by a.date desc, a.name limit 20 offset %s", [nextoffset])
    link_list = RssArtistIndex.objects.raw("SELECT id, a.name, a.link, a.domain, a.date, a.title, count(*) as numArtist FROM rss_artist_index a where a.blacklist = 0 and date >= curdate() - INTERVAL DAYOFWEEK(curdate())+6 DAY group by a.link order by date desc limit 20 offset %s", [nextoffset])
    print "ending link_list"
    #predictions = []
    #for e in RssArtistIndex.objects.raw("SELECT id, a.name, a.link, b.content, a.domain, a.date, a.title, count(*) as numArtist FROM rss_artist_index a join rss_content b on a.link = b.link where a.blacklist = 0 group by a.link order by a.date desc, a.name limit 20 offset %s", [nextoffset]):
       #predictions.append(predict(e.content))
    #print predictions
    print "starting trending"
    trending = RssArtistIndex.objects.raw("SELECT id, a.name, count(*)-1 as total from rss_artist_index a join artist_cache b on a.name = b.name where type='Good' and date >= curdate() - INTERVAL DAYOFWEEK(curdate())+6 DAY group by a.name having count(*) > 1 order by count(*) desc limit 20")
    print "ending trending"
    print "starting date"
    date = RssArtistIndex.objects.raw("SELECT id, DATE(a.date) FROM rss_artist_index a group by DATE(a.date) order by a.date desc limit 1")
    print "ending date"
    '''
    cursor = connections['data'].cursor()
    print "starting row"
    cursor.execute("SELECT count(*) from (SELECT * FROM rss_artist_index  where blacklist = 0 group by link) temp")
    #cursor.execute("SELECT count(*) from (SELECT id, a.name, a.link, a.domain, a.date, a.title, count(*) as numArtist FROM rss_artist_index a where a.blacklist = 0 group by a.link order by a.date desc, a.name) temp")
    row = cursor.fetchone()
    cursor.close()
    '''
    row = {}
    row[0] = 1000
    print nextoffset
    print row[0]
    if nextoffset+20 >= row[0]:
        if nextoffset == 0:
            previousoffset = nextoffset
            nextoffset = nextoffset
        else:
            previousoffset = nextoffset - 20
            nextoffset = nextoffset
    else:
        if nextoffset == 0:
            previousoffset = 0
        else:
            previousoffset = nextoffset - 20
        nextoffset = nextoffset + 20
    print "ending row"
    context = {'link_list':link_list,
                #'predictions' : predictions,
                'trending':trending,
                'date' :date,
                'offset':nextoffset,
                'previousoffset':previousoffset,
        }
    return render(request, 'art/index.html', context)
def get_query_cols(string):
    m = re.findall( 'select(.*?)from', string, re.DOTALL)
    m = m[0].split(",")
    final = []
    for a in m:
        new = a.replace("distinct", "")
        new = new.strip()
        new = new.split(".")
        if len(new) > 1:
            new = new[1]
        else:
            new = new[0]
        new = new.title()
        test = new.lower().find(" as ")
        if test != -1:
            new = new[test+4:]
        final.append(new)
    return final

def top(request):
    if "id" in request.GET:
        lists = Toplists.objects.filter(id = int(request.GET['id']))
        query = lists[0].query
        conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
        cur = conn.cursor()
        cur.execute("SET NET_WRITE_TIMEOUT = 28800")
        #cur.execute("select count(*) from rss_artist_index where name = %s", (name))
        cur.execute(query)
        results = cur.fetchall()
        cur.close()
        conn.close()
        columns = get_query_cols(query)
        output = []
        for result in results:
            new = {}
            for i in range(0,len(result)):
                temp = result[i]
                if isinstance(temp, basestring):
                    temp = temp.title()
                new[columns[i]] = temp
            output.append(new)
        data = json.dumps(output)
        return HttpResponse(data)

    tops = Toplists.objects.all()
    context = {
            'tops' : tops,
    }
    return render(request, 'art/top.html', context)



def stats(request):
    artist_total = ArtistScore.objects.raw("SELECT 0 as name,count(*) as total from artist_score")
    #artist_total = RssArtistIndex.objects.raw("SELECT id, count(distinct name) as total FROM rss_artist_index a where a.blacklist = 0")
    artist_rankings = ArtistScore.objects.raw("SELECT b.name as id, b.name, b.rank as artfacts_rank, a.score FROM artist_score a join artfacts b on a.name = b.name")
    #artist_rankings = {}
    start_date = (dt.date.today() - dt.timedelta(1*365/12)).strftime('%Y-%m-%d %H:%M:%S')
    feed_total = RssArtistIndex.objects.raw("SELECT 0 as id, count(*) as total FROM rss_artist_index a")
    live_feed = RssArtistIndex.objects.raw("SELECT id, date, domain, count(*) as total FROM rss_artist_index a where a.blacklist = 0 and date > %s group by DATE(date), domain having total < 500",[start_date] )
    #live_feed = {}
    artists_per_day = RssArtistIndex.objects.raw("SELECT id, date, count(distinct name) as total FROM rss_artist_index a where a.blacklist = 0 and date > %s GROUP BY DATE(date) order by date desc",[start_date] )
    #artists_per_day = {}
    articles_per_day = RssArtistIndex.objects.raw("SELECT id, date, count(distinct link) as total FROM rss_artist_index a where a.blacklist = 0 and date > %s GROUP BY DATE(date) order by date desc",[start_date] )
    #articles_per_day = {}
    article_percentage = RssContent.objects.raw("select link, a.domain, count(*) * 100 / (select count(*) from rss_content) as percent from rss_content a group by domain ;");
    #article_percentage = {}
    #select a.domain, ((select count(*) from rss_content a1 join svm_art b1 on a1.link = b1.link where b1.prediction="Art" and a1.domain = a.domain)/count(*)) as art, ((select count(*) from rss_content a1 join svm_art b1 on a1.link = b1.link where b1.prediction="Music" and a1.domain = a.domain)/count(*)) as music, ((select count(*) from rss_content a1 join svm_art b1 on a1.link = b1.link where b1.prediction="News" and a1.domain = a.domain)/count(*)) as news from rss_content a group by a.domain order by art asc
#    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
#    cur = conn.cursor()
#    cur.execute("SET NET_WRITE_TIMEOUT = 28800")
    ranks = ArtistScore.objects.raw("select distinct b.name, b.score from artist_cache a join artist_score b on a.name = b.name where type='Good' and score > 1000 order by score desc limit 15")
#    cur.close()
#   conn.close()
#    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
#    cur = conn.cursor()
#    cur.execute("SET NET_WRITE_TIMEOUT = 28800")
    unknownranks= ArtistScore.objects.raw("select distinct b.name, b.score from artist_cache a join artist_score b on a.name = b.name where a.name not in (select name from artfacts) and type=\"Good\" order by score desc limit 15")
#    unknownranks = cur.fetchall()
#    cur.close()
#    conn.close()
#    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
#    cur = conn.cursor()
#    cur.execute("SET NET_WRITE_TIMEOUT = 28800")
    top1000 = ArtistScore.objects.raw("select distinct b.name, b.score, c.rank from artist_cache a join artist_score b on a.name = b.name join artfacts as c on a.name = c.name where c.rank > 1000 and type='Good' order by score desc limit 15")
#    top1000 = cur.fetchall()
#    cur.close()
#    conn.close()
#    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
#    cur = conn.cursor()
#    cur.execute("SET NET_WRITE_TIMEOUT = 28800")
    top10000 = ArtistScore.objects.raw("select b.name, b.score, c.rank from artist_cache a join artist_score b on a.name = b.name join artfacts c on a.name = c.name where c.rank > 10000 and type=\"Good\" group by b.name order by score desc limit 15")
#    top10000 = cur.fetchall()
#    cur.close()
   # conn.close()
   # conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
#    cur = conn.cursor()
#    cur.execute("SET NET_WRITE_TIMEOUT = 28800")
    top100000 = ArtistScore.objects.raw("select distinct b.name, b.score, c.rank from artist_cache a join artist_score b on a.name = b.name join artfacts c on a.name = c.name where c.rank > 100000 and type=\"Good\" order by score desc limit 15")
#    top100000 = cur.fetchall()
#    cur.close()
#    conn.close()
    #select a.domain, (count(*)/(select count(*) from rss_content a1 where a1.domain = a.domain)) from rss_content a join svm_art b on a.link = b.link where b.prediction = "Art" group by a.domain
    context = {
                'live_feed':live_feed,
                'feed_total':feed_total,
                'artist_total':artist_total,
                'artist_rankings':artist_rankings,
                'artists_per_day' : artists_per_day,
                'articles_per_day': articles_per_day,
                'article_percentage': article_percentage,
                'top10' : ranks,
                'top10unknown' : unknownranks,
                'top1000':top1000,
                'top10000':top10000,
                'top100000':top100000,
                }
    return render(request, 'art/stats.html', context)

def clean(request):
    clean = RssContent.objects.raw("SELECT distinct a.link, a.domain, a.date, a.title, a.content from rss_content a join rss_artist_index b on a.link = b.link where name not in (select artistName from artist_top10k) order by date desc;")
    context = {
                'link_list' : clean
                }
    return render(request, 'art/clean.html', context)

def trash(request):
    artist_list = RssArtistIndex.objects.raw("SELECT id, a.link, a.domain, a.date, a.title FROM rss_artist_index a where a.blacklist = 1 order by a.date desc limit 150")
    context = {'link_list':artist_list,
                }
    return render(request, 'art/trash.html', context)

def getScore(request):
    name = request.GET['name']
    du.get_score(name)
    return HttpResponse(get_score(None, name))
    #conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
    #cur = conn.cursor()
    #cur.execute("SET NET_WRITE_TIMEOUT = 28800")
    ##cur.execute("select count(*) from rss_artist_index where name = %s", (name))
    #cur.execute("select count(*) from rss_content a join svm_art b on a.link = b.link where prediction = 'Art' and match(content) against('\"" + name + "\"' in boolean mode)")
    #results = cur.fetchall()
    #artist_score = results[0][0]
    #artist_score = RssArtistIndex.objects.filter(name=name).count()
    #return HttpResponse(artist_score)

def getDomain(request):
    name = request.GET['name']
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800")
    #cur.execute("select count(*) from rss_artist_index where name = %s", (name))
    cur.execute("select a.domain from (select distinct a.domain from rss_content a join svm_art b on a.link = b.link where prediction = 'Art' and match content against('\"" + name + "\"' in boolean mode)) a join domain_google g on a.domain = g.domain order by wikiart desc limit 5")
    results = cur.fetchall()
    cur.close()
    conn.close()
    #artist_domain = RssArtistIndex.objects.filter(name=name).values('domain').distinct()
    data = json.dumps(list(results))
    return HttpResponse(data)

def getArtistGallery(request):
    name = request.GET['name']
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800")
    #cur.execute("select count(*) from rss_artist_index where name = %s", (name))
    cur.execute("select distinct g.name, X(g.location), Y(g.location) from gallery g join artist_cache a on a.gallery_id = g.id where a.name = %s", (name, ))
    results = cur.fetchall()
    cur.close()
    conn.close()
    #artist_domain = RssArtistIndex.objects.filter(name=name).values('domain').distinct()
    data = json.dumps(list(results))
    return HttpResponse(data)

def getLink(request):
    #artist_all = RssArtistIndex.objects.raw("SELECT id, a.link, a.domain, b.date, a.title FROM rss_artist_index a join rss_content b on a.link=b.link where name=%s and a.blacklist = 0 group by a.link order by b.date desc", [name])
    name = request.GET['name']
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800")
    #cur.execute("select count(*) from rss_artist_index where name = %s", (name))
    #cur.execute("select a.link, domain, date, title, content from rss_content a join svm_art b on a.link = b.link where prediction = 'Art' and (match(content) against('\"" + name + "\"' in boolean mode) or match(title) against('\"" + name + "\"' in boolean mode)) and blacklist = 0 order by date desc")
    cur.execute("select a.link, domain, date, title, content from rss_content a join svm_art b on a.link = b.link where prediction = 'Art' and match(content,title) against('\"" + name + "\"' in boolean mode) and blacklist = 0 order by date desc")
    results = cur.fetchall()
    all_results = []
    for r in results:
        tmp = {}
        tmp[0] = r[0]
        tmp[1] = r[1]
        tmp[2] = r[2]
        tmp[3] = r[3]
        tmp[4] = r[4]
        tmp[5], tmp[6] = du.get_final_article_score(name, tmp[3], tmp[4], tmp[1], tmp[2], tmp[0])
        all_results.append(tmp)
    cur.close()
    conn.close()
    """
    new_results = []
    for result in results:
        content = result[4]
        if name in result[3]:
            new_results.append(result)
        percent = content.indexOf(name)/len(content)
        if percent < 90:
            new_results.append(result)
    """
    data = json.dumps(list(all_results), cls=DateTimeEncoder)
    #data = serializers.serialize("json", results)
    return HttpResponse(data)

def getTrash(request):
    #name = request.GET['name']
    #artist_all = RssArtistIndex.objects.raw("SELECT id, a.link, a.domain, b.date, a.title FROM rss_artist_index a join rss_content b on a.link=b.link where name=%s and a.blacklist = 1 group by a.link order by b.date desc", [name])
    #data = serializers.serialize("json", artist_all)
    #return HttpResponse(data)
    #artist_all = RssArtistIndex.objects.raw("SELECT id, a.link, a.domain, b.date, a.title FROM rss_artist_index a join rss_content b on a.link=b.link where name=%s and a.blacklist = 0 group by a.link order by b.date desc", [name])
    name = request.GET['name']
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800")
    #cur.execute("select count(*) from rss_artist_index where name = %s", (name))
    cur.execute("select a.link, domain, date, title from rss_content a join svm_art b on a.link = b.link where prediction = 'Art' and match(content) against('\"" + name + "\"' in boolean mode) and blacklist = 1 order by date desc")
    results = cur.fetchall()
    cur.close()
    conn.close()

    data = json.dumps(list(results), cls=DateTimeEncoder)
    #data = serializers.serialize("json", results)
    return HttpResponse(data)

def getLatest(request):
    artist_all = RssArtistIndex.objects.raw("SELECT id, a.link, a.domain, b.date, a.title FROM rss_artist_index a join rss_content b on a.link=b.link where blacklist = 0 group by a.link order by b.date desc limit 300")
    data = serializers.serialize("json", artist_all)
    return HttpResponse(data)

def getArtist(request):
    url = request.GET['url']
    url = urllib2.unquote(url.decode("utf8"))
    name = urllib2.unquote(request.GET['name'].decode("utf-8"))
    artist_all = RssArtistIndex.objects.raw("SELECT id, name FROM rss_artist_index where link = %s and name <> %s order by name desc", [url, name])
    data = serializers.serialize("json", artist_all, fields=("name"))
    return HttpResponse(data)

def search(request):
    name = request.GET['q']
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800;")
    cur.execute("(SELECT a.name as name FROM artfacts a where name RLIKE %s) union (select distinct(name) as name from artist_cache where type=\"Good\" and name RLIKE %s)", ('[[:<:]]' + name + '[[:>:]]', '[[:<:]]' + name + '[[:>:]]',))
    rows = cur.fetchall()
    cur.close()
    conn.close()
    artist_list = []
    for row in rows:
        temp = {}
        temp['name'] = row[0]
        artist_list.append(temp);

    context = {'link_list':artist_list,}

    return render(request, 'art/search.html', context)

def removeLinkArtist(request):
    url = request.GET['url']
    name = urllib2.unquote(request.GET['name'].decode("utf-8"))
    url = urllib2.unquote(url.decode("utf8"))
    cursor = connections['data'].cursor()
    cursor.execute("UPDATE rss_artist_index set blacklist=1 where link = %s and name = %s", [url,name])
    cursor.close()
    return HttpResponse(url + " " + name)

def restoreLinkArtist(request):
    url = request.GET['url']
    name = urllib2.unquote(request.GET['name'].decode("utf-8"))
    url = urllib2.unquote(url.decode("utf8"))
    cursor = connections['data'].cursor()
    cursor.execute("UPDATE rss_artist_index set blacklist=0 where link = %s and name = %s", [url,name])
    cursor.close()
    return HttpResponse(url)

def removeLink(request):
    url = request.GET['url']
    url = urllib2.unquote(url.decode("utf8"))
    cursor = connections['data'].cursor()
    cursor.execute("UPDATE rss_artist_index set blacklist=1 where link = %s", [url])
    cursor.execute("UPDATE rss_content set blacklist=1 where link = %s", [url])
    cursor.close()
    return HttpResponse(url)

def restoreLink(request):
    url = request.GET['url']
    url = urllib2.unquote(url.decode("utf8"))
    cursor = connections['data'].cursor()
    cursor.execute("UPDATE rss_artist_index set blacklist=0 where link = %s", [url])
    cursor.execute("UPDATE rss_content set blacklist=0 where link = %s", [url])
    cursor.close()
    return HttpResponse(url)

def getMore(request):
    url = request.GET['url']
    url = urllib2.unquote(url.decode("utf8"))
    cursor = connections['data'].cursor()
    cursor.execute("Select * from rss_content where original_link = %s", [url])
    row = cursor.fetchone()
    artists = []
    cursor.execute("select artistName from artist_top10k")
    rows = cursor.fetchall()
    for r in rows:
        name = r[0].split(" ")
        if len(name) <= 1:
            continue
        if len(r[0]) < 4:
            continue
        if len(name[0]) <= 2 or len(name[1]) <= 2:
            continue
        if r[0] in row[6]:
            artists.append(r[0])

    artists_json = json.dumps(artists)
    cursor.close()
    return HttpResponse(artists_json)

@csrf_exempt
def svm(request):
    #text = request.post['text']
    return render(request, 'art/svm.html')
    #return httpresponse(url)

def fetch_20newsgroups(SOURCES, categories=None, shuffle=True, random_state=42):
    data_lst = list()
    target = list()
    filenames = list()
    for query, classification in SOURCES:
        conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
        cur = conn.cursor()
        cur.execute("SET NET_WRITE_TIMEOUT = 28800")
        cur.execute(query)
        results = cur.fetchall()
        cur.close()
        conn.close()
        for result in results:
            data = result[0]
            data_lst.append(data)
            target.append(classification)
            filenames.append(categories[classification])


    data = {}
    data['data'] = data_lst
    data['target'] = np.array(target)
    data['filenames'] = np.array(filenames)
    data['description'] = 'the 20 newsgroups by date dataset'

    data['target_names'] = list(categories)
    if categories is not None:
        labels = [(data['target_names'].index(cat), cat) for cat in categories]
        # Sort the categories to have the ordering of the labels
        labels.sort()
        labels, categories = zip(*labels)
        mask = np.in1d(data['target'], labels)
        data['filenames'] = data['filenames'][mask]
        data['target'] = data['target'][mask]
        # searchsorted to have continuous labels
        data['target'] = np.searchsorted(labels, data['target'])
        data['target_names'] = list(categories)
        # Use an object array to shuffle: avoids memory copy
        data_lst = np.array(data['data'], dtype=object)
        data_lst = data_lst[mask]
        data['data'] = data_lst.tolist()

    if shuffle:
        random_state = check_random_state(random_state)
        indices = np.arange(data['target'].shape[0])
        random_state.shuffle(indices)
        data['filenames'] = data['filenames'][indices]
        data['target'] = data['target'][indices]
        # Use an object array to shuffle: avoids memory copy
        data_lst = np.array(data['data'], dtype=object)
        data_lst = data_lst[indices]
        data['data'] = data_lst.tolist()
    return data

def check_naermes(article, SOURCES):
    names = []
    for query, classification in SOURCES:
        conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
        cur = conn.cursor()
        cur.execute("SET NET_WRITE_TIMEOUT = 28800")
        cur.execute(query)
        results = cur.fetchall()
        cur.close()
        conn.close()
        for artist in results:
                try:
                    temp = artist[0].split()
                    if len(temp) >=2:
                        index = article.find(artist[0])
                        if index != -1:
                            #if not article[index+len(artist[0])].isalpha():
                            names.append((artist[0], classification))
                except Exception as e:
                    continue

    return names
            #data = result[1]
            #data_lst.append(data)
            #target.append(classification)
            #filenames.append(categories[classification])

def accumulate(l, categories):
    it = itertools.groupby(l, operator.itemgetter(1))
    for key, subiter in it:
       yield categories[key], sum(1 for item in subiter)

def predict(text):
    ART = 0
    MUSIC = 1
    NEWS = 2
    NONE = "none"
    CATEGORIES = ["Art", "Music", "News"]
    SOURCES = [
            ("select article from clean_articles where type='art' limit 100;", ART),
            ("select article from clean_articles where type='music' limit 100;", MUSIC),
            ("select article from clean_articles where type='news' limit 100;", NEWS),
    ]
    SOURCES_NAMES = [
            ("select name from artfacts;", ART),
            ("select artistName from artist_top10k;", MUSIC),
    ]
    if os.path.exists("text_clf.pkl"):
        text_clf = joblib.load('text_clf.pkl')
        return CATEGORIES[int(text_clf.predict([text])[0])]
    else:
        data_train = fetch_20newsgroups(SOURCES, categories=CATEGORIES, shuffle=True, random_state=42)
        categories = data_train['target_names']    # for case categories == None
        def size_mb(docs):
            return sum(len(s.encode('utf-8')) for s in docs) / 1e6

        data_train_size_mb = size_mb(data_train['data'])

        print("%d documents - %0.3fMB (training set)" % (
            len(data_train['data']), data_train_size_mb))
        #print("%d documents - %0.3fMB (test set)" % (
            #len(data_test.data), data_test_size_mb))
        print("%d categories" % len(categories))
        print()
        text_clf = Pipeline([('vect', CountVectorizer(strip_accents="ascii", stop_words="english")),
                             ('tfidf', TfidfTransformer()),
                             ('clf', SGDClassifier(loss='hinge', penalty='l2', alpha=1e-3, n_iter=5, random_state=42)),
                             #('clf', LinearSVC()),
        ])

        text_clf = text_clf.fit(data_train['data'], data_train['target'])
        joblib.dump(text_clf, 'text_clf.pkl')
        return CATEGORIES[int(text_clf.predict([text])[0])]

@csrf_exempt
def svm_test(request):
    #print request
    #return HttpResponse("test")
    text = request.POST['text']
    #print text
    ART = 0
    MUSIC = 1
    NEWS = 2
    NONE = "none"
    CATEGORIES = ["Art", "Music", "News"]
    SOURCES = [
            ("select article from clean_articles where type='art' limit 100;", ART),
            ("select article from clean_articles where type='music' limit 100;", MUSIC),
            ("select article from clean_articles where type='news' limit 100;", NEWS),
    ]
    SOURCES_NAMES = [
            ("select name from artfacts;", ART),
            ("select artistName from artist_top10k;", MUSIC),
    ]

    words = "".join((char if char.isalnum() else " ") for char in text).split()
    #print words
    additions = {}
    for query, classification in SOURCES:
        data_lst = list()
        conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
        cur = conn.cursor()
        cur.execute("SET NET_WRITE_TIMEOUT = 28800")
        cur.execute(query)
        results = cur.fetchall()
        cur.close()
        conn.close()
        for result in results:
            data = result[0]
            data_lst.append(data)
        vectorizer = TfidfVectorizer(min_df=1, stop_words = 'english', strip_accents="ascii")
        X = vectorizer.fit_transform(data_lst)
        idf = vectorizer.idf_
        temp = dict(zip(vectorizer.get_feature_names(), idf))
        sorted_x = sorted(temp.items(), key=operator.itemgetter(1))
        #print "\n\n"
        #print CATEGORIES[classification]
        #print "\n\n"
        #additions[classification] = 0
        #for word in words:
            #if word.lower() in temp:
                #print word.lower()
                #additions[classification] += (10 - temp[word.lower()])
                #print temp[word.lower()]
        ##+ " " + str(sorted_x[word])
    #print additions

    names = check_names(text, SOURCES_NAMES)
    cat_counts = list(accumulate(names, CATEGORIES))
    svm = False
    string = ""
    if len(cat_counts) == 1:
        print("Names found")
        #svm = True
        string += cat_counts[0][0] + " Name: " + names[0][0]
    elif len(cat_counts) == 0:
        svm = True
    else:
        svm = True



    if os.path.exists("text_clf.pkl"):
        text_clf = joblib.load('text_clf.pkl')
        print(text_clf.predict(text))
        print(text_clf.predict([text]))
        string += "Category is: "
        string += CATEGORIES[int(text_clf.predict([text])[0])]
        string += "<br>"
        string += "Categories: <br>"
        string += "[" + ", ".join(CATEGORIES) + "]<br>"
        string += "Decision: "
        string += np.array_str(text_clf.decision_function([text]))
        return HttpResponse(string)
    else:
        data_train = fetch_20newsgroups(SOURCES, categories=CATEGORIES, shuffle=True, random_state=42)
        categories = data_train['target_names']    # for case categories == None
        def size_mb(docs):
            return sum(len(s.encode('utf-8')) for s in docs) / 1e6

        data_train_size_mb = size_mb(data_train['data'])

        print("%d documents - %0.3fMB (training set)" % (
            len(data_train['data']), data_train_size_mb))
        #print("%d documents - %0.3fMB (test set)" % (
            #len(data_test.data), data_test_size_mb))
        print("%d categories" % len(categories))
        print()
        text_clf = Pipeline([('vect', CountVectorizer(strip_accents="ascii", stop_words="english")),
                             ('tfidf', TfidfTransformer()),
                             ('clf', SGDClassifier(loss='hinge', penalty='l2', alpha=1e-3, n_iter=5, random_state=42)),
                             #('clf', LinearSVC()),
        ])

        text_clf = text_clf.fit(data_train['data'], data_train['target'])
        joblib.dump(text_clf, 'text_clf.pkl')
        print "prediction"
        print(text_clf.predict([text]))
        print "decision"
        print(text_clf.decision_function([text]))
        string += "Category is: "
        string += CATEGORIES[int(text_clf.predict([text])[0])]
        string += "<br>"
        string += "Categories: <br>"
        string += "[" + ", ".join(CATEGORIES) + "]<br>"
        string += "Decision: "
        string += np.array_str(text_clf.decision_function([text]))
        return HttpResponse(string)

@csrf_exempt
def svm_add(request):
    #text = request.POST['text']
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800")
    cur.execute("select count(*) from clean_articles where type=%s;", ("art", ))
    art = cur.fetchone()[0]
    cur.close()
    cur = conn.cursor()
    cur.execute("select count(*) from clean_articles where type=%s;", ("music", ))
    music = cur.fetchone()[0]
    cur.close()
    cur = conn.cursor()
    cur.execute("select count(*) from clean_articles where type=%s;", ("movie", ))
    movie = cur.fetchone()[0]
    cur.close()
    cur = conn.cursor()
    cur.execute("select count(*) from clean_articles where type=%s;", ("news", ))
    news = cur.fetchone()[0]
    cur.close()
    conn.close()
    context = {'art':art,
                'music':music,
                'movie' :movie,
                'news':news,
        }

    return render(request, 'art/svm_add.html', context)
    #return HttpResponse(url)

@csrf_exempt
def svm_add_article(request):
    print request
    #return HttpResponse("test")
    text = request.POST['text']
    cat = request.POST['cat']
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
    conn.autocommit(True)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800;")
    cur.execute("INSERT INTO clean_articles (article, type) VALUES (%s, %s)", (text, cat))
    cur.close()
    conn.close()
    return HttpResponse("success")

@csrf_exempt
def svm_get_article(request):
	cat = request.POST['cat']
	conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
	cur = conn.cursor()
	cur.execute("SET NET_WRITE_TIMEOUT = 28800")
	cur.execute("select * from clean_articles where type = %s", (cat, ))
	articles = cur.fetchall()
	articles_json = json.dumps(articles)
	cur.close()
	conn.close()
	return HttpResponse(articles_json)

def crawl(seeds, pattern, depth=1):
    print "\n\nstarting crawl with pattern: " + pattern
    frontier = seeds
    visited_urls = set()
    final_url = []
    for crawl_url in frontier:
        #print "Crawling:", crawl_url[0]
        visited_urls.add(crawl_url[0])
        try:
            headers = { 'User-Agent' : random.choice(ua)}
            req = urllib2.Request(crawl_url[0], None, headers)
            c = urllib2.urlopen(req)
        except:
            print "Could not access", crawl_url
            continue
        content_type = c.info().get('Content-Type')
        if not content_type.startswith('text/html'):
            print "Not html"
            continue
        html = c.read()
        #with open("soup.html", "w") as f:
        #    f.write(html)
        soup = BeautifulSoup(html)
        discovered_urls = set()
        links = soup.find_all('a')
        #print links
        links = soup('a')    # Get all anchor tags
        #print "soup"
        #print links
        #print "Found links: "
        #print links
        #print crawl_url
        if crawl_url[1] < depth:
            #print "curr depth: " + str(crawl_url[1])
            #print "links"
            #print links
            for link in links:
                #print "processing: "
                #print link
                if ('href' in dict(link.attrs)):
                    url = urljoin(crawl_url[0], link['href'])
                    inFrontier = False
                    for f in frontier:
                        if url == f[0]:
                            inFrontier = True

                    if (url not in visited_urls
                            and url not in discovered_urls and not inFrontier):
                        domain = urlparse(url).netloc
                        #print "Domain: " + domain
                        for f in frontier:
                            if domain in f[0]:
                                #print "matching url: " + url + " with " + domain
                                patternre = re.compile(pattern)
                                #new_url = url.replace("http://", "")
                                #new_url = new_url.replace("https://", "")
                                if patternre.match(url):
                                    #print url + " matched pattern"
                                    discovered_urls.add(url)
                                    final_url.append(url)
                                #else:
                                    #print url + " didnt match pattern"
            for new_url in discovered_urls:
                frontier.append((new_url, crawl_url[1]+1))
            #frontier += discovered_urls
        #else:
            #break

    #print "-"*20
    return final_url
        #time.sleep(2)
def getArtistLink(html):
    #print html
    #soup = BeautifulSoup(html)
    #html = soup.renderContents()
    #print html

    #with open("alink.html", "w") as f:
        #f.write(html)
    links = None
    try:
        hxs = HtmlXPathSelector(text=html)
        #try:
        links = hxs.select("//a[translate(normalize-space(.), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')='artists']/@href").extract()
        print "Get Artist links: "
        print links
        if links:
            print "returning 1"
            return links
        links = hxs.select("//a[translate(normalize-space(.), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')='artist']/@href").extract()
        print "Get Artist links2: "
        print links
        if links:
            print "returning 2"
            return links
        #links = hxs.select("//*[contains(@href,'artists') and not(contains(@href, '@'))]/@href").extract()
        #print "Get Artist link3"
        #print links
        #if links:
            #print "returning 3"
            #return links
        links = hxs.select("//*[contains(@href,'directory') and not(contains(@href, '@'))]/@href").extract()
        print "Get Artist link4"
        print links
        if links:
            print "returning 4"
            return links


    except:
        try:
            links = hxs.select("//*[contains(@href,'artists')]/@href").extract()
            print "Get Artist link5"
            print links
            return links
        except:
            return None
    #except:
        #link = hxs.select("//a[text()='Artist']/@href").extract()[0]
        #print link
        #if "artist" in link.lower():
            #return link
    return links

def save_artists(url, good, bad, scores):
    print "in save artist"
    print scores
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
    conn.autocommit(True)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800")
    try:
        cur.execute("select id from gallery where website = %s", (url, ))
        gallery_id = cur.fetchall()[0][0]
    except:
        print cur._last_executed
        raise
    cur.close()
    for g in good:
        cur = conn.cursor()
        #TODO Need to fix unicode error
        try:
            cur.execute("INSERT INTO artist_cache (gallery_id, name, type) values (%s, %s, %s)", (gallery_id, g, "Good"))
            conn.commit()
            #cur.execute("INSERT IGNORE INTO artist_score (name, score) values (%s, %s)", (g, scores[g]))
            #conn.commit()
        except Exception as e:
            print e
            continue
            #print cur._last_executed
            #raise
        #scores[name] = cur.fetchone()[0]
        cur.close()
    for b in bad:
        cur = conn.cursor()
        try:
            cur.execute("INSERT INTO artist_cache (gallery_id, name, type) values (%s, %s, %s)", (gallery_id, b, "Bad"))
            conn.commit()
        except Exception as e:
            print e
            continue
            #print cur._last_executed
            #raise
        #scores[name] = cur.fetchone()[0]
        cur.close()
    conn.close()

def save_new_gallery(name, place_id, address, website, lat, lon, manual=False):
    print name, place_id, address, website, lat, lon
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True)
    conn.autocommit(True)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800;")
    cur.execute("select id from gallery where place_id = %s or (X(location) = %s and Y(location) = %s)", (place_id, lat, lon))
    rows = cur.fetchall()
    print rows
    if len(rows) == 0:
        try:
            cur.execute("INSERT INTO gallery (name, place_id, address, website, location) VALUES (%s, %s, %s, %s, GeomFromText(%s))", (name, place_id, address, website, "POINT(" + str(lat) + " " +  str(lon) + ")"))
            conn.commit()
        except Exception as e:
            print e
            print cur._last_executed
            return 2
            #raise

        cur.close()
        conn.close()
        print "gallery not found"
        if manual:
            return 0
        return -1
        #row = cur.fetchall()[0][0]
    else:
        if manual:
            return 1
        cur.execute("select * from artist_cache where gallery_id = %s", (rows[0][0], ))
        num_artists = cur.fetchall()
        cur.close()
        conn.close()
        if len(num_artists) == 0:
            print "no artists found in gallery"
            return -1
        else:
            print "found gallery with artists"
            return rows[0][0]
    #else:
        #try:
            #cur.execute("select id from gallery where place_id = %s", place_id)
            #conn.commit()
        #except:
            #print cur._last_executed
            #raise
        #row = cur.fetchall()[0][0]
        #try:
            #cur.execute("UPDATE gallery set name = %s, address = %s, website = %s, location = GeomFromText(%s) where id = %s", (name, place_id, address, website, "POINT(" + lat + " " + lon + ")", row))
            #conn.commit()
        #except:
            #print cur._last_executed
            #raise


    #return row
    #return HttpResponse("success")
def get_article_score(name, title, content):
    if name.lower() in title.lower():
        return 10
    if name.lower()in content.lower():
        temp = name.split()
        fcount = content.lower().count(temp[0])
        lcount = content.lower().count(temp[1])
        if fcount > 5 or lcount > 5:
            return 10
        if fcount > 4 or lcount > 4:
            return 8
        if fcount > 3 or lcount > 3:
            return 6
        if fcount > 2 or lcount > 2:
            return 4
        if fcount > 1 or lcount > 1:
            return 2
        return 1
    else:
        return 0

def get_score(request, name=None):
    get_score = False
    if not name:
        try:
            print request
            name = request.GET['name']
        except Exception as e:
            print e
            return render(request, 'art/name.html')
    else:
        get_score = True


    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
    cur = conn.cursor()
    cur.execute("SELECT score FROM artist_score where name=%s", (name, ))
    score = cur.fetchall()
    if len(score) != 0 and get_score:
        return score[0][0]
    cur.close()
    cur = conn.cursor()
    cur.execute("select domain, title, content from rss_content a join svm_art b on a.link = b.link where prediction = 'Art' and match(content) against('\"" + name + "\"' in boolean mode)")
    articles = cur.fetchall()
    cur.close()
    article_score = []
    domainscore = []
    average = 0L
    for article in articles:
        #get domain score
        cur = conn.cursor()
        cur.execute("select wikiart from domain_google where domain = %s", (article[0], ))
        dscore = cur.fetchall()
        if len(dscore) == 0:
            try:
                count = GoogleSearch("\"%s\" site:wikipedia.org"%(domain[0]), use_proxy=False).count()
                time.sleep(5)
                countart = GoogleSearch("\"%s\",art site:wikipedia.org"%(domain[0]), use_proxy=False).count()
                cur = conn.cursor()
                print "domain: %s, wiki:%s, wikiart:%s"%(domain[0], count, countart)
                try:
                    cur.execute("INSERT INTO domain_google (domain, wiki, wikiart) VALUES (%s, %s, %s)", (domain[0], count, countart))
                    conn.commit()
                except:
                    print cur._last_executed
                    raise
                cur.close()
                time.sleep(5)
                cur = conn.cursor()
                cur.execute("select wikiart from domain_google where domain = %s", (article[0], ))
                dscore = cur.fetchall()[0][0]
            except:
                dscore = 1
        else:
            dscore = dscore[0][0]

        domainscore.append(dscore)
        cur.close()
        print "dscore: %s"%(dscore)
        ascore = get_article_score(name, article[1], article[2])
        print "ascore: %s"%(ascore)
        try:
            article_score.append((dscore,math.log(dscore), ascore))
            average = average+(math.log(dscore)*ascore)
        except:
            article_score.append((dscore,0L, ascore))

    if len(article_score) != 0:
        average = average/len(article_score)*math.log(len(articles))
    cur = conn.cursor()
    cur.execute("INSERT IGNORE INTO artist_score (name, score) values (%s, %s)", (name, average))
    conn.commit()
    cur.close()
    conn.close()
    if get_score:
        return average
    else:
        return HttpResponse("(Domain Score, Log(Domain), Article Score)<br>%s, <br>Average*log(numarticles): %s"%(",<br>".join(str(x) for x in article_score), average))

def get_artist_score(names):
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
    #cur.execute("SET NET_WRITE_TIMEOUT = 28800")
    print "getting content"
    #cur.execute("select link, content, original_date, title from rss_content")
    #cur.execute("select count(*) from clean_articles where type = %s", (cat))
    #rows = cur.fetchall()
    #print "getting content"
    scores = {}

    for name in names:
        name = re.sub(r'[^\w\s]','',name)
        print "getting score for " + name
        scores[name] = get_score(None, name)

    for name in names:
        cur = conn.cursor()
        cur.execute("select * from artfacts a where name = %s", (name, ))
        row = cur.fetchall()
        cur.close()
        if len(row) == 0:
            cur = conn.cursor()
            cur.execute("select * from google_search a where name = %s", (name, ))
            row = cur.fetchall()
            cur.close()
            if len(row) == 0:
                try:
                    count = GoogleSearch(name, use_proxy=False).count()
                    cur = conn.cursor()
                    try:
                        cur.execute("INSERT INTO google_search (name, count) VALUES (%s, %s)", (name, count))
                        conn.commit()
                    except:
                        print cur._last_executed
                        raise
                    cur.close()
                    time.sleep(1)
                except:
                    continue

    sorted_scores = OrderedDict(sorted(scores.items(), key=operator.itemgetter(1), reverse=True))
    print sorted_scores
    conn.close()
    return sorted_scores

def get_rendered_html(url, btype = 0):
    print "Getting Rendered HTML for url: " + url
    with open("/home/home/Documents/main_website/rendered.txt", "a") as f:
    	f.write(url+"\n")
    #p = subprocess.Popen('pkill -f phantomjs', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    #for line in p.stdout.readlines():
            #print line,
    #retval = p.wait()
    #display = Display(visible=0, size=(800, 600))
    #display.start()
    #chromedriver = '/usr/bin/chromedriver'
    #os.environ["webdriver.chrome.driver"] = chromedriver
    html = ""
    with pyvirtualdisplay.Display(visible=False):
        if btype == 0:
            #binary = FirefoxBinary("/usr/bin/firefox", log_file="firefox.log")
            #firefox_profile = webdriver.FirefoxProfile()
            #firefox_profile.set_preference('permissions.default.stylesheet', 2)
            #firefox_profile.set_preference('permissions.default.image', 2)
            #firefox_profile.set_preference('dom.ipc.plugins.enabled.libflashplayer.so', 'false')
            #browser = webdriver.Firefox(firefox_profile, binary)
        #elif btype == 1:
            #browser = webdriver.Chrome()
        #elif btype == 2:
            browser = webdriver.PhantomJS(service_log_path='/tmp/ghostdriver.log', service_args=["--webdriver-loglevel=ERROR"])
        browser.get(url)
        sleep(10)
        html = browser.page_source
        #with open("test.html", "w") as f:
        #    f.write(html.encode('utf8'))
        browser.close()
        browser.quit()
        #browser.service.process.send_signal(signal.SIGTERM)
    #display.stop()
    if isinstance(html, str):
        print "*"*5 + "string is ascii"
        html=html.replace("&nbsp;", " ");
    elif isinstance(html, unicode):
        print "*"*5 + "string is unicode"
        html=html.replace(u'\xa0', u' ');
    return html
    #print orig_html
    #return browser.page_source

def get_html(url):
    #browser = webdriver.Firefox()
    #browser.get(url)
    #return browser.page_source
    print "Getting HTML"
    headers = { 'User-Agent' : random.choice(ua)}
    req = urllib2.Request(url, None, headers)
    try:
        c = urllib2.urlopen(req)
        response = c.read()
    except:
        print "Could not access", url
        return None
    html = response
    #html = html.replace("<span>", "")
    #html = html.replace("</span>", "")
    if isinstance(html, str):
        print "*"*5 + "string is ascii"
        try:
            html=html.replace("&nbsp;", " ")
            html = unicode(html, errors='ignore')
        except Exception as e:
            print e
            pass
    elif isinstance(html, unicode):
        print "*"*5 + "string is unicode"
        try:
            html=html.replace(u'\xa0', u' ');
        except Exception as e:
            print e
            pass
    return html

def parse_wix(html):
    terms = re.findall('(?<=")[ \t\n]*[A-Z]+[a-zA-Z0-9&]+ {1}[A-Z]+[a-zA-Z0-9&]+[ \t\n]*(?=")', html)
    #print "unfiltered terms"
    #print terms
    #exclude terms which are in lists
    #option_terms = re.findall("(?<=>)[ \t\n]*[a-zA-Z0-9-&.]+ {1}[a-zA-Z0-9-&.]+[ \t\n]*(?=</option>)", html)
    #menu_terms = re.findall("(?<=<)(?:.*)(?<=menu)(?:.*)(?<=>)[ \t\n]*[a-zA-Z0-9-&.]+ {1}[a-zA-Z0-9-&.]+[ \t\n]*(?=<)", html)
    #print "menu_terms"
    #print menu_terms
    final_terms = []
    for term in terms:
        #if term not in option_terms:
            #if not any(term in x for x in menu_terms):
        if term not in final_terms:
            final_terms.append(term.strip())
    final_terms = list(set(final_terms))
    return final_terms

def parse_terms(html):
    #html = html.replace("&nbsp;","")
    #terms = re.findall("(?<=>)[ \t\n]*[a-zA-Z&,]+[ \t\n]+[a-zA-Z&]+[ \t\n]*(?=<)", html)
    #terms_pattern = "(?<=>)[ \t\n]*(?:[a-zA-Z&,]|[^\W\d_])+(?:[ \t\n]|&nbsp;)+(?:[a-zA-Z&,]|[^\W\d_])+[ \t\n]*(?=<)"
    terms_pattern = "(?<=>)[ \t\n]*(?:[A-Z]){1}(?:[a-zA-Z&,]|[^\W\d_])+(?:[ \t\n]|&nbsp;)+(?:[A-Z]){1}(?:[a-zA-Z&,]|[^\W\d_])+[ \t\n]*(?=<)"
    unicode_terms = re.compile(terms_pattern, re.UNICODE)
    terms = unicode_terms.findall(html)
    print "unfiltered terms"
    print terms
    #exclude terms which are in lists
    option_terms = re.findall("(?<=>)[ \t\n]*[a-zA-Z0-9-&.]+ {1}[a-zA-Z0-9-&.]+[ \t\n]*(?=</option>)", html)
    menu_terms = re.findall("(?<=<)(?:.*)(?<=menu)(?:.*)(?<=>)[ \t\n]*[a-zA-Z0-9-&.]+ {1}[a-zA-Z0-9-&.]+[ \t\n]*(?=<)", html)
    if len(menu_terms) > 5:
        menu_terms = []
    #nav_terms = re.findall("(?<=<)(?:.*)(?<=nav)(?:.*)(?<=>)[ \t\n]*[a-zA-Z0-9-&.]+ {1}[a-zA-Z0-9-&.]+[ \t\n]*(?=<)", html)
    nav_terms = re.findall("<(?:[^<>]*)(?<=nav)(?:[^<>]*)>[ \t\n]*[a-zA-Z0-9-&.]+ {1}[a-zA-Z0-9-&.]+[ \t\n]*(?=<)", html)
    if len(nav_terms) > 5:
        nav_terms = []
    print "menu_terms"
    print menu_terms
    print "nav_terms"
    print nav_terms
    print "option_terms"
    print option_terms
    final_terms = []
    for term in terms:
        if term not in option_terms:
            if not any(term in x for x in menu_terms):
                if not any(term in x for x in nav_terms):
                    if term not in final_terms:
                        final_terms.append(term.strip())

    bad_terms = []
    for term in terms:
        if term in option_terms:
            bad_terms.append(term)
        if any(term in x for x in menu_terms):
            bad_terms.append(term)
        if any(term in x for x in nav_terms):
            bad_terms.append(term)

    final_terms = list(set(final_terms))
    bad_terms = list(set(bad_terms))
    print "final_terms"
    print final_terms
    print "bad_terms"
    print bad_terms
    return (final_terms, bad_terms)

def run_xpath(terms, html):
    if len(html) > 300000:
        return ([], [])
    #check = 0
    #for term in terms:
        #if "art " in term.lower() or "artist" in term.lower():
            #pass
        #else:
            #check+=1
    #print "check"
    #print check
    #if check < 8:
        #return ([], [])
    #print terms
    #for term in terms:
        #term_arr = term.split(' ')
        #if d.check(term_arr[0]) and d.check(term_arr[1]):
            #print "removing term: " + term
            #terms.remove(term)
    #print terms
    ART = 0
    MUSIC = 1
    NEWS = 2
    NONE = "none"
    CATEGORIES = ["Art", "Music", "News"]

    SOURCES = [
            ("select article from clean_articles where type='art' limit 100;", ART),
    ]

    sorted_x = []
    for query, classification in SOURCES:
        data_lst = list()
        conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
        cur = conn.cursor()
        cur.execute("SET NET_WRITE_TIMEOUT = 28800")
        cur.execute(query)
        results = cur.fetchall()
        cur.close()
        conn.close()
        for result in results:
            data = result[0]
            data_lst.append(data)
        vectorizer = TfidfVectorizer(min_df=1, stop_words = 'english', strip_accents="ascii")
        X = vectorizer.fit_transform(data_lst)
        idf = vectorizer.idf_
        temp = dict(zip(vectorizer.get_feature_names(), idf))
        sorted_x = sorted(temp.items(), key=operator.itemgetter(1))
        sorted_x = sorted_x[0:200]

    common_bad = []
    for x in sorted_x:
        common_bad.append(x[0])

    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
    cur = conn.cursor()
    cur.execute("select name, count(*) from artist_cache where type='Bad' group by name having count(*) > 4 order by count(*) desc")
    #names= []
    rows = cur.fetchall()
    for row in rows:
        words = row[0].split()
        for word in words:
            common_bad.append(word.lower())

    print "common_bad"
    print common_bad
    cur.close()
    conn.close()

    #with open("curr.html", "w") as f:
    #    try:
    #        f.write(html)
    #    except:
    #        f.write(html.encode('utf8'))

    hxs = HtmlXPathSelector(text=html)
    print "HTML Length: " + str(len(hxs.select("/*").extract()[0]))
    xpaths = []
    xpaths_same_node = []
    done = []
    for term in terms:
        xpaths.append("contains(text(), '" + term + "')")
        xpaths_same_node.append("contains(., '" + term + "')")

    #detect errors in ancestor
    errors = {}

    heads = []
    doc_lens = []
    counts = []
    #ids = []
    #_id = 0
    error_count = 0
    for i in range(0, len(xpaths) - 1):
        for j in range(0, len(xpaths) - 1):
            if xpaths[i] != xpaths[j]:
                if xpaths[i] in errors and errors[xpaths[i]] > 2:
                    continue
                if xpaths[j] in errors and errors[xpaths[j]] > 2:
                    continue

                try:
                    xpath_string = "//*[" + xpaths[i] + "]/ancestor::*[count(. | //*[" + xpaths[j] + "]/ancestor::*) = count(//*[" + xpaths[j] + "]/ancestor::*)][1]"
                    select = hxs.select(xpath_string)
                    temp1 = select.extract()
                    temp = temp1[0]
                except Exception as e:
                    #try:
                        #xpath_string = "//*[" + xpaths_same_node[i] + " and " + xpaths_same_node[j] + "]"
                        #select = hxs.select(xpath_string)
                        #temp1 = select.extract()
                        #temp = temp1[0]
                    #except Exception as e:
                    print "error in ancestor"
                    print xpath_string
                    print xpaths[i]
                    print xpaths[j]
                    print e
                    error_count += 1
                    if error_count > len(terms)*.5:
                        return ([], [])
                    if xpaths[i] not in errors:
                        errors[xpaths[i]] = 1
                    else:
                        errors[xpaths[i]] += 1

                    if xpaths[j] not in errors:
                        errors[xpaths[j]] = 1
                    else:
                        errors[xpaths[j]] += 1

                    continue
                #reject too short and reject full page html
                if str(len(hxs.select("/*").extract()[0])) == len(temp):
                    continue
                    #print xpaths[i] + " " + xpaths[j]
                if temp not in heads:
                    heads.append(temp)
                    doc_lens.append(len(temp))
                    count = 0
                    for term in terms:
                        if term in temp:
                            count += 1
                    counts.append(count)
                    print "Length: " + str(len(temp)) + ", Count: " + str(count)
                    #ids.append(_id)
                    #_id += 1

    #for head in heads:
        #print "Length: " + str(len(head)) + ", Count: " + str(count)



    max_count = -1
    max_length = -1
    try:
        max_count = max(counts)
        max_length = max(doc_lens)
    except:
        pass
    length = len(heads)
    for h, l, c in zip(heads, doc_lens, counts):
        if (max_count*2)/length < max_count/2.0:
            print "MAX COUNT: " + str(max_count/2.0) + " LENGTH: " + str(length)
            if l < max_length*.05:
                print "MAX LENGTH: " + str(max_length*.05)
                #heads.remove(h)
                #doc_lens.remove(l)
                #counts.remove(c)
        print '-'*20
        print "Length: " + str(l) + ", Count: " + str(c)
        #print h[0:100]
        #print "...."
        #print h[(len(h)-100):]
        temp = []
        for term in terms:
            if term in h:
                temp.append(term)
        print temp
        print '-'*20

    #print zip(heads, doc_lens, counts)
    heads_info = sorted(zip(heads, doc_lens, counts), key=lambda tup: tup[2])

    good = []
    bad = []
    for term in terms:
        if "art " in term.lower() or "artist" in term.lower():
            bad.append(term)
    curr_count = 0
    for h in heads_info:
        if h[2] < 3:
            for term in terms:
                if term in h[0]:
                    if term not in bad:
                        #print "Bad"
                        #print bad
                        #print "Good"
                        #print good
                        print "Too less terms in head: " + term
                        bad.append(term)
            curr_count = h[2]
            continue

        new_count = 0
        for term in terms:
            if term not in bad and term not in good and term in h[0]:
                new_count+=1
        #diff_count = h[2] - curr_count
        if new_count <= 2:
            for term in terms:
                if term in h[0]:
                    if term not in bad:
                        if term not in good:
                            #print "Bad"
                            #print bad
                            #print "Good"
                            #print good
                            print "Head has too little new terms: " + term + " " + str(h[2])
                            bad.append(term)
            curr_count = h[2]
            continue

        bad_section = False
        bad_section_count = 0
        bad_section_term = []
        for term in terms:
            if term in h[0]:
                for common in common_bad:
                    beg = re.compile('^' + common + " ")
                    end = re.compile(" " + common + '$')
                    #if (common+" ") in term.lower() or (" " + common) in term.lower():
                    if beg.search(term.lower()) is not None or end.search(term.lower()) is not None:
                        bad_section_count += 1
                        bad_section_term.append(term)
                        if bad_section_count == 3:
                            bad_section = True
                        else:
                            bad.append(term)


        for term in terms:
            if term in h[0]:
                if bad_section:
                    print "Head has too many bad terms: " + term + "::"
                    print bad_section_term
                    bad.append(term)
                elif term not in good and term not in bad:
                    good.append(term)


    if len(good) <= 3:
        return ([], [])
    print "Final List"
    print '-' * 20
    print "BAD TERMS"
    print '-' * 20
    print bad
    print '-' * 20
    print "GOOD TERMS"
    print '-' * 20
    print good
    print '-' * 20

    return (good, bad)

def random_date():
    """
    This function will return a random datetime between two datetime
    objects.
    """
    start = (dt.datetime.now())
    end = (dt.datetime.now() + dt.timedelta(days=365.25/2))
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = randrange(int_delta)
    return start + timedelta(seconds=random_second)

def update_next(request):
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True)
    conn.autocommit(True)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800;")
    random_next_update = random_date()
    cur.execute("update gallery set next_update = %s where place_id = %s and website = %s", (random_next_update, request.POST['place_id'], request.POST['url']))

@csrf_exempt
def gallery_get(request, new=False, names_only=False):
    if names_only:
        original_url = request.GET['link']
        url = request.GET['link']
    else:
        update_next(request)
        original_url = request.POST['url']
        url = request.POST['url']

    html = get_html(url)
    pattern = "((http|https):\/\/)?[a-zA-Z0-9!#$%&'()*+,\-.:;<=>?_`~\/]+\/[a-zA-Z0-9!#$%&'()*+,\-.:;<=>?_`~]*artist[a-zA-Z0-9!#$%&'()*+,\-.:;<=>?^_`]*(\/)?$"
    final_urls = crawl([(url, 0)],pattern)
    if len(final_urls) == 0:
        print "Trying without http"
        pattern = "(?<=href=\")[a-zA-Z0-9!#$%&'()*+,\-.:;<=>?_`~\/]*artist[a-zA-Z0-9!#$%&'()*+,\-.:;<=>?^_`]*(\/)?"
        #pattern = "href=\"[a-zA-Z0-9!#$%&'()*+,\-.:;<=>?_`~\/]*artist[a-zA-Z0-9!#$%&'()*+,\-.:;<=>?^_`]*(\/)?\""
        #pattern = "[a-zA-Z0-9!#$%&'()*+,\-.:;<=>?_`~\/]*artist[a-zA-Z0-9!#$%&'()*+,\-.:;<=>?^_`]*(\/)?$"
        final_urls = crawl([(url, 0)],pattern)
        if len(final_urls) == 0:
            print "Trying without http and directory"
            pattern = "[a-zA-Z0-9!#$%&'()*+,\-.:;<=>?_`~\/]*directory[a-zA-Z0-9!#$%&'()*+,\-.:;<=>?^_`]*(\/)?$"
            final_urls = crawl([(url, 0)],pattern)

    #print html
    print "FOUND URLS:"
    print final_urls
    if len(final_urls) == 0:
        #try rendered html
        #html = get_rendered_html(url)
        print "No URLS Found, trying artist url"
        artistLink = getArtistLink(html)
        print artistLink
        if artistLink:
            print "Artist Link Found"
            print artistLink
            for aLink in artistLink:
                print "link in alink: " + aLink
                if "http://" not in aLink:
                    aLink = urljoin(url, aLink)
                    print url
                    print aLink
                final_urls.append(aLink)
        else:
            print "Artist Link Not Found"
            html = get_rendered_html(url)
            artistLink = getArtistLink(html)
            if artistLink:
                for aLink in artistLink:
                    if "http://" not in aLink:
                        aLink = urljoin(url, aLink)
                        print aLink
                    final_urls.append(aLink)
            else:
                return HttpResponse("No Artist Link Found")
    else:
        artistLink = getArtistLink(html)
        if artistLink:
            print "Artist Link Found 1"
            for aLink in artistLink:
                if "http://" not in aLink:
                    aLink = urljoin(url, aLink)
                if aLink not in final_urls:
                    final_urls.append(aLink)
    print "Using Following URLS"
    print final_urls
    for i in final_urls:
        if "http" not in i:
            final_urls.remove(i)
    good = []
    bad = []
    for url in final_urls:
        print "original_url"
        print url
        print original_url
        print urljoin(original_url, "#")
        if url == original_url or url == original_url + "#":
            continue
        print "current url"
        print final_urls
        print url
        html = get_html(url)
        if not html:
            print "No HTML Found"
            html = get_rendered_html(url)
            if not html:
                print "No Rendered HTML Found"
                continue
        #print html
        #with open("test.html", "w") as f:
            #f.write(html)
        (terms, bad_terms) = parse_terms(html)
        try:
            if len(terms) <= 2:
                html = get_rendered_html(url)
                (terms, bad_terms) = parse_terms(html)
                #with open("tmp.html", "w") as f:
                    #f.write(html)
                print "firefox"
                print terms
                if len(terms) <= 2:
                    terms = parse_wix(html)
                    print "wix"
                    print terms
                    if not terms:
                        continue
        except:
            continue
        print "Got Following Terms:"
        print terms
        temp = run_xpath(terms,html)
        #try:
        bad += bad_terms
        if temp:
            (g, b) = temp
            if g == [] and b == []:
                html = get_rendered_html(url)
                temp = run_xpath(terms,html)

            if g:
                good+=g
            if b:
                bad+=b
    #return HttpResponse("Bad: [" + ", ".join(bad) + "]<br/><br/>" + "Good: [" + ", ".join(good) + "]")
    print "URLS"
    print url
    print '-' * 20
    print final_urls
    print '-' * 20
    #score = []
    #for g in good:
    bad = list(set(bad))
    good = list(set(good))
    for b in bad:
        if b in good:
            bad.remove(b)
    scores = get_artist_score(good)
    result = []
    for k in scores:
        temp = [k, scores[k], get_artist_status(None, k)]
        result.append(temp)

    if new:
        save_artists(original_url, good, bad, scores)
    if names_only:
        return json.dumps(result)
    return HttpResponse(json.dumps(result))
        #return HttpResponse("" + "<br>".join(['%s::%s' % (key, value) for (key, value) in scores.items()]) + "")
    #return httpresponse(url)

@csrf_exempt
def gallery(request):
    #text = request.post['text']
    return render(request, 'art/gallery.html')
    #return httpresponse(url)

@csrf_exempt
def location(request):
    try:
        text = request.GET['test']
        return render(request, 'art/location_test.html')
    except:
        return render(request, 'art/location.html')

def get_artists_from_gallery(gallery_id):
    print "getting gallery with id: " + str(gallery_id)
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
    #cur = conn.cursor()
    #cur.execute("select name from artist_cache where gallery_id = %s and type = 'Good'", (gallery_id, ))
    #names = cur.fetchall()
    #for name in names:
    #    get_score(None, name[0])
    #cur.close()
    cur = conn.cursor()
    cur.execute("select c.name, score from artist_cache c join artist_score s on c.name = s.name where gallery_id = %s and type = 'Good' order by score desc", (gallery_id, ))
    names= []
    rows = cur.fetchall()
    scores = {}
    for row in rows:
        scores[row[0]] = row[1]
    cur.close()
    scores = OrderedDict(sorted(scores.items(), key=operator.itemgetter(1), reverse=True))
    #scores = get_artist_score(names)
    conn.close()
    return scores
    #return
    return HttpResponse("" + "<br>".join(['%s::%s' % (key, value) for (key, value) in scores.items()]) + "")
    #return names

@csrf_exempt
def save_gallery(request):
    url = request.POST['url']
    name = request.POST['name']
    place_id = request.POST['place_id']
    address = request.POST['address']
    lat = request.POST['lat']
    lon = request.POST['lon']
    gallery_id = save_new_gallery(name,place_id, address, url, lat, lon)
    #if gallery_id == -1:
        #return gallery_get(request, True)
    #else:
        #print "getting artists from gallery: " + name
        #return get_artists_from_gallery(gallery_id)
    return HttpResponse(gallery_id)

def check_gallery(request):
    print "check_gallery"
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True)
    conn.autocommit(True)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800;")
    cur.execute("select next_update from gallery where place_id = %s and website = %s", (request.POST['place_id'], request.POST['url']))
    rows = cur.fetchall()
    next_update = rows[0][0]
    print "next_update!!!!: %s"%(next_update)
    if next_update > dt.date.today():
        return False
    else:
        return True
    #if len(rows) == 0:

def get_gallery_mapping(gallery_id):
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
    cur = conn.cursor()
    cur.execute("select mapping from gallery where id = %s", (gallery_id, ))
    mapping = cur.fetchall()[0][0]
    cur.close()
    conn.close()
    return mapping

@csrf_exempt
def get_gallery_names(request):
    gallery_id = request.POST['gallery_id']
    if gallery_id == -1:
        print "gallery_id -1"
        if check_gallery(request):
            return gallery_get(request, True)
        else:
            return HttpResponse('[]')
    else:
        #print "getting artists from gallery: " + name
        mapping = get_gallery_mapping(gallery_id)
        if mapping != -1:
            gallery_id = mapping
        artists = get_artists_from_gallery(gallery_id)
        if not artists:
            if check_gallery(request):
                return gallery_get(request, True)
            else:
                return HttpResponse('[]')
            #return gallery_get(request, True)
        else:
            result = []
            for k in artists:
                temp = [k, artists[k], get_artist_status(None, k)]
                result.append(temp)
            return HttpResponse(json.dumps(result))
    return HttpResponse("")

@csrf_exempt
def demo(request):
    os.system("killall phantomjs")
    return render(request, 'art/demo.html')

def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
    return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)
def get_galleries(request):
    lat = 40.742352
    lng = -74.006210
    DEFAULT_RADIUS = 5
    cache = None
    google = None
    factual = None
    get_names = True
    try:
        print request
        cache = request.GET['cached']
    except Exception as e:
        try:
            google = request.GET['google']
        except:
            try:
                factual = request.GET['factual']
            except:
                return render(request, 'art/name.html')

    try:
        lat = request.GET['lat']
        lng = request.GET['lng']
    except Exception as e:
        print e
    exists = False
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
    conn.autocommit(True)
    cur = conn.cursor()
    cur.execute("SELECT lat, lng FROM daily_location_update")
    rows = cur.fetchall()
    for row in rows:
        if isclose(row[0], float(lat), rel_tol=.01, abs_tol=0.0) and isclose(row[1], float(lng), rel_tol=.01, abs_tol=0.0):
            exists = True

    cur.close()
    conn.close()
    if not exists:
        conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
        conn.autocommit(True)
        cur = conn.cursor()
        cur.execute("INSERT INTO daily_location_update (`lat`, `lng`, `date`, `daily`, `desc`) VALUES (%s, %s, CURRENT_TIMESTAMP, 0, %s);", (lat, lng, 'Location on ' + time.strftime("%d/%m/%Y")))
        cur.close()
        conn.close()

    if cache:
        if get_names:
            conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
            cur = conn.cursor()
            cur.execute("SET @lat = %s", (lat, ))
            cur.execute("SET @lon = %s", (lng, ))
            cur.execute("SET @rad = %s", (DEFAULT_RADIUS, ))
            cur.execute("""\
            SELECT g.id, g.name, g.website, g.x, g.y, g.distance, g.c, g.place_id, g.address, c.name, a.score, a.status FROM
                (SELECT
                    id, g.name, website, X(location) AS x,Y(location) AS y, (
                      3959 * acos (
                      cos ( radians( @lat ) )
                      * cos( radians( X(location) ) )
                      * cos( radians( Y(location) ) - radians( @lon ) )
                      + sin ( radians( @lat ) )
                      * sin( radians( X(location) ) )
                    )
                ) AS distance, count(*) AS c, place_id, address
                FROM gallery g LEFT JOIN artist_cache a ON g.id = a.gallery_id
                    WHERE a.type='Good'
                GROUP BY g.id
                HAVING distance < @rad
                ORDER BY c DESC) AS g
            JOIN
                (SELECT * FROM artist_cache WHERE type="Good") AS c ON g.id = c.gallery_id
            JOIN artist_score a ON c.name = a.name""")
            cached_locations = cur.fetchall()
            print cached_locations
            cur.close()
            conn.close()
            data = json.dumps(cached_locations)
            return HttpResponse(data)

        conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
        cur = conn.cursor()
        cur.execute("SET @lat = %s", (lat, ))
        cur.execute("SET @lon = %s", (lng, ))
        cur.execute("SET @rad = %s", (DEFAULT_RADIUS, ))
        cur.execute("""\
        SELECT
            id, g.name, website, X(location),Y(location), (
              3959 * acos (
              cos ( radians( @lat ) )
              * cos( radians( X(location) ) )
              * cos( radians( Y(location) ) - radians( @lon ) )
              + sin ( radians( @lat ) )
              * sin( radians( X(location) ) )
            )
        ) AS distance, count(*) AS c, place_id, address
        FROM gallery g LEFT JOIN artist_cache a ON g.id = a.gallery_id
	    WHERE a.type='Good'
        GROUP BY g.id
        HAVING distance < @rad
	    ORDER BY c DESC""")
        #cur.execute("select id, name, website, X(location), Y(location) from gallery")
        cached_locations = cur.fetchall()
        print cached_locations
        cur.close()
        conn.close()
        data = json.dumps(cached_locations)
        return HttpResponse(data)
    if google:
       API_KEY = "AIzaSyCPfugPVQjJ40XVL_m3s7V-rMOQ1HpHZCw"
       API_KEY2 = "AIzaSyBfH2wBi5LR6ZBZjGiQjhHf3F12BpPoAP0"
       try:
           google_places = GooglePlaces(API_KEY)
           next_page_token = None
           google_locations = []
           for i in range(0, 3):
               sleep(2)
               if i == 0:
                   print "query with lat: %s lng: %s"%(lat, lng)
                   query_result = google_places.nearby_search(
                       lat_lng={'lat': lat, 'lng': lng},
                       name = "gallery",
                       types = types.TYPE_ART_GALLERY,)
               elif next_page_token:
                   print "query with next_page %s"%(next_page_token)
                   query_result = google_places.nearby_search(
                           next_page_token=next_page_token)
               next_page_token = query_result.next_page_key
               print "next_page_token: %s"%(next_page_token)
               galleries = []
               for place in query_result.places:
                   temp = []
                   place.get_details()
                   print place.geo_location
                   if place.website == None:
                       continue
                   try:
                       gallery_id = save_new_gallery(place.name,place.place_id, place.formatted_address, place.website, place.geo_location['lat'], place.geo_location['lng'])
                   except:
                       continue
                   temp.append(gallery_id)
                   temp.append(place.name)
                   temp.append(place.website)
                   temp.append(place.geo_location['lat'])
                   temp.append(place.geo_location['lng'])
                   temp.append(place.place_id)
                   google_locations.append(temp)
       except Exception as e:
           google_places = GooglePlaces(API_KEY2)
           next_page_token = None
           google_locations = []
           for i in range(0, 3):
               sleep(2)
               if i == 0:
                   print "query with lat: %s lng: %s"%(lat, lng)
                   query_result = google_places.nearby_search(
                       lat_lng={'lat': lat, 'lng': lng},
                       name = "gallery",
                       types = types.TYPE_ART_GALLERY,)
               elif next_page_token:
                   print "query with next_page %s"%(next_page_token)
                   query_result = google_places.nearby_search(
                           next_page_token=next_page_token)
               next_page_token = query_result.next_page_key
               print "next_page_token: %s"%(next_page_token)
               galleries = []
               for place in query_result.places:
                   temp = []
                   place.get_details()
                   print place.geo_location
                   if place.website == None:
                       continue
                   try:
                       gallery_id = save_new_gallery(place.name,place.place_id, place.formatted_address, place.website, place.geo_location['lat'], place.geo_location['lng'])
                   except:
                       continue
                   temp.append(gallery_id)
                   temp.append(place.name)
                   temp.append(place.website)
                   temp.append(place.geo_location['lat'])
                   temp.append(place.geo_location['lng'])
                   temp.append(place.place_id)
                   google_locations.append(temp)


       data = json.dumps(google_locations, cls=DecimalEncoder)
       return HttpResponse(data)
    if factual:
       API_KEY = '3leY3SOw4EnuDcGf6K1VidS6ncdaAZg2t5RxATPl'
       API_SECRET = 'tU6JxyJA3UOvfvAOFxczMJiRcz1eQXSKk3URsVHi'
       f = Factual(API_KEY, API_SECRET)
       places = f.table('places')
       factual_locations = []
       for i in range(0, 10):
           #data = places.filters({"$and":[{"country":{"$eq":"US"}},{"region":{"$eq":"NY"}},{"locality":{"$eq":"NEW YORK"}},{"category_labels":{"$includes":'["SOCIAL","ARTS","ART DEALERS AND GALLERIES"]'}}]}).offset(i*50).limit(50).data()
           data = places.filters({"category_labels":{"$includes":'["SOCIAL","ARTS","ART DEALERS AND GALLERIES"]'}}).geo(circle(lat, lng, 5000)).offset(i*50).limit(50).data()
           for place in data:
               if 'website' not in place:
                   continue
               temp = []
               try:
                   gallery_id = save_new_gallery(place['name'],place['factual_id'],place['address'] + ", " +  place['locality'] + ", " + place['region'] + "," +  place['postcode'] + "," + place['country'], place['website'], place['latitude'], place['longitude'])
               except:
                   continue
               temp.append(gallery_id)
               temp.append(place['name'])
               temp.append(place['website'])
               temp.append(place['latitude'])
               temp.append(place['longitude'])
               temp.append(place['factual_id'])
               factual_locations.append(temp)

       data = json.dumps(factual_locations)
       return HttpResponse(data)

def get_artist_status(request, name=None):
    if not name:
        name = request.GET['name']
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
    cur = conn.cursor()
    cur.execute("select count(*) from artfacts where name = %s", (name, ))
    name_count = cur.fetchall()[0][0]
    cur.close()
    conn.close()
    if name_count > 0:
        return "Known"
    else:
        return "Unknown"

def get_artist_from_website(request):
    url = request.GET['link']
    status = 0
    error = 0
    if error == 1:
        status = 1
        data = {}
        data['status'] = status
        data['debug'] = str(e)
        data['message'] = "Error with link"
        json_data = json.dumps(data)
        return HttpResponse(json_data)

    data = {}
    data['status'] = status
    data['message'] = "Successfully Scraped URL: " + url
    data['artists'] = "test"
    data['artists'] = gallery_get(request, names_only=True)
    json_data = json.dumps(data)
    return HttpResponse(json_data)

def settings(request):
    """
    Loads add feed html page which redirects to tinyrss to add feed

    **Template:**

    :template: 'art/add_feed.html'

    """
    updated_score = ArtistScore.objects.raw("select name,updated from artist_score order by updated desc limit 1")
    context = {
        'updated_score':updated_score,
    }
    return render(request, 'art/add_feed.html', context)

def add_gallery(request):
    name = request.GET['name']
    place_id = request.GET['placeid']
    address = request.GET['address']
    website = request.GET['website']
    lat = request.GET['lat']
    lng = request.GET['lng']
    status = save_new_gallery(name,place_id, address, website, lat, lng, True)
    return HttpResponse(status)

def remove_gallery(request):
    gallery = request.GET['gallery']
    force = request.GET['force']
    status = 0
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True)
    conn.autocommit(True)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800;")

    if force == "0":
        cur.execute("select name from artist_cache where gallery_id = %s", (gallery, ))
        rows = cur.fetchall()
        if len(rows) > 0:
            status = 2
            return HttpResponse(status)
        cur.close()
    #else:
    #   try:
    #        cur.execute("DELETE FROM artist_cache where gallery_id = %s;", (gallery, ))
    #    except Exception as e:
    #        print e
    #        print cur._last_executed
    #        status = 1
    #        return HttpResponse(status);
    #    cur.close()
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800;")
    try:
        cur.execute("DELETE FROM gallery where id = %s;", (gallery, ))
    except Exception as e:
        print e
        print cur._last_executed
        status = 1
    cur.close()
    conn.close()
    return HttpResponse(status)

def search_gallery(request):
    gallery = request.GET['gallery']
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800;")
    cur.execute("select id, name, place_id, address, website, X(location), Y(location) from gallery where lower(name) like %s", (gallery + "%",))
    rows = cur.fetchall()
    cur.close()
    conn.close()

    data = json.dumps(rows)
    return HttpResponse(data)

def update_gallery(request):
    status = 0
    error = ""
    id = request.GET['id']
    name = request.GET['name']
    place_id = request.GET['placeid']
    address = request.GET['address']
    website = request.GET['website']
    lat = request.GET['lat']
    lng = request.GET['lng']
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True)
    conn.autocommit(True)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800;")
    cur.execute("select * from gallery where id = %s", (id,))
    rows = cur.fetchall()
    print rows
    if len(rows) != 0:
        try:
            cur.execute("update gallery set name=%s, place_id=%s, address=%s, website=%s, location=POINT(%s,%s) where id =  %s", (name, place_id, address, website, lat, lng, id))
        except Exception as e:
            status = 1
            error = "Error updating gallery" + str(e)
    else:
        status = 1
        error = "Gallery Does Not Exist"

    cur.close()
    conn.close()
    json_data = {
        "status":status,
        "error":error,
    }
    data = json.dumps(json_data)

    return HttpResponse(data)



def search_artist(request):
    artist = request.GET['artist']
    gallery = request.GET['gallery']
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800;")
    cur.execute("select name from artist_cache where gallery_id = %s and lower(name) like %s", (gallery, artist + "%",))
    rows = cur.fetchall()
    cur.close()
    conn.close()

    data = json.dumps(rows)
    return HttpResponse(data)

def search_artist_all(request):
    artist = request.GET['artist']
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800;")
    cur.execute("select distinct(name) from artist_cache where lower(name) like %s", (artist + "%",))
    rows = cur.fetchall()
    cur.close()
    conn.close()

    data = json.dumps(rows)
    return HttpResponse(data)

def search_artfacts(request):
    artist = request.GET['artist']
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800;")
    cur.execute("select name from artfacts where lower(name) like %s", (artist + "%",))
    rows = cur.fetchall()
    cur.close()
    conn.close()

    data = json.dumps(rows)
    return HttpResponse(data)

def add_artist(request):
    artist = request.GET['artist']
    gallery = request.GET['gallery']
    status = 0
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True)
    conn.autocommit(True)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800;")
    cur.execute("select name from artist_cache where name = %s and gallery_id = %s and type='Good'", (artist, gallery,))
    rows = cur.fetchall()
    cur.close()
    if len(rows) > 0:
        status = 1
        return HttpResponse(status)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800;")
    try:
        cur.execute("INSERT INTO artist_cache (gallery_id, name, type) VALUES (%s, %s, 'Good')", (gallery, artist))
    except Exception as e:
        print e
        print cur._last_executed
        status = 2
        return HttpResponse(status)
    cur.close()
    conn.close()
    try:
        get_score(None, artist)
    except:
        pass
    return HttpResponse(status)

def add_artist_test(request):
    artist = request.GET['artist']
    gallery = request.GET['gallery']
    status = 0
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True)
    conn.autocommit(True)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800;")
    cur.execute("select name from artist_cache where name = %s and gallery_id = %s and type='Good'", (artist, gallery,))
    rows = cur.fetchall()
    cur.close()
    if len(rows) > 0:
        status = 1
        return HttpResponse(status)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800;")
    cur.close()
    conn.close()
    try:
        get_score(None, artist)
    except:
        pass
    return HttpResponse(status)

def remove_artist(request):
    artist = request.GET['artist']
    gallery = request.GET['gallery']
    status = 0
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True)
    conn.autocommit(True)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800;")
    try:
        cur.execute("DELETE FROM artist_cache WHERE gallery_id=%s and name=%s", (gallery, artist))
    except Exception as e:
        print e
        print cur._last_executed
        status = 1
        return HttpResponse(status)
    cur.close()
    conn.close()
    return HttpResponse(status)

def remove_artist_all(request):
    artist = request.GET['artist']
    status = 0
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True)
    conn.autocommit(True)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800;")
    try:
        cur.execute("DELETE FROM artist_cache WHERE name=%s", (artist,))
    except Exception as e:
        print e
        print cur._last_executed
        status = 1
        return HttpResponse(status)
    cur.close()
    conn.close()
    return HttpResponse(status)

def remove_artfacts(request):
    artist = request.GET['artist']
    status = 0
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True)
    conn.autocommit(True)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800;")
    try:
        cur.execute("DELETE FROM artfacts WHERE name=%s", (artist, ))
    except Exception as e:
        print e
        print cur._last_executed
        status = 1
        return HttpResponse(status)
    cur.close()
    conn.close()
    return HttpResponse(status)

def get_article(request):
    url = request.GET['link']
    status = 0
    try:
        hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
            'Accept-Encoding': 'none',
            'Accept-Language': 'en-US,en;q=0.8',
            'Connection': 'keep-alive'}
        cj = CookieJar()
        opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
        opener.addheaders = [
            ('User-Agent', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11'),
            ('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'),
            ('Accept-Charset', 'ISO-8859-1,utf-8;q=0.7,*;q=0.3'),
            ('Accept-Encoding', 'none'),
            ('Accept-Language', 'en-US,en;q=0.8'),
            ('Connection', 'keep-alive')
            ]
        response = opener.open(url)
        #req = urllib2.Request(url, headers=hdr)
        #response = urllib2.urlopen(req)
    except Exception as e:
        status = 1
        data = {}
        data['status'] = status
        data['debug'] = str(e)
        data['message'] = "Error with link"
        json_data = json.dumps(data)
        return HttpResponse(json_data)
    orig_html = response.read()
    cleaner = Cleaner()
    cleaner.javascript = True
    cleaner.style = True
    html = cleaner.clean_html(orig_html)
    g = Goose();
    text = ""
    article = g.extract(url=url,raw_html=html)
    if re.sub(r'\s+', '', article.cleaned_text.strip()) == "":
        extractor = Extractor(extractor='ArticleExtractor', html=html)
        extracted_text = extractor.getText()
        if re.sub(r'\s+', '', extracted_text.strip())  == "":
            status = 2
            data = {}
            data['status'] = status
            data['message'] = "Error with link"
            json_data = json.dumps(data)
            return HttpResponse(json_data)
        else:
            text = extracted_text
    else:
        text = article.cleaned_text

    if text == "":
        status = 3
        data = {}
        data['status'] = status
        data['message'] = "Error with link"
        json_data = json.dumps(data)
        return HttpResponse(json_data)


    soup = BeautifulSoup(orig_html)

    title = soup.title.string

    data = {}
    data['status'] = status
    data['message'] = "Successfully Scraped URL: " + url
    data['title'] = title
    data['article'] = text
    json_data = json.dumps(data)
    return HttpResponse(json_data)

@csrf_exempt
def add_article(request):
    link = request.POST['link']
    date = request.POST['date']
    title = request.POST['title']
    article = request.POST['article']
    status = 0
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True)
    conn.autocommit(True)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800;")
    cur.execute("select link from rss_content where link = %s or original_link = %s", (link, link,))
    rows = cur.fetchall()
    cur.close()
    if len(rows) > 0:
        status = 1
        return HttpResponse(status);
    extracted = tldextract.extract(link)
    domain = "{}.{}".format(extracted.domain, extracted.suffix)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800;")
    try:
        cur.execute("INSERT INTO rss_content (link, original_link, domain, date, original_date, title, content, blacklist, tag ) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, 'Manual')", (link, link, domain, date, date, title, article, 0,))
    except Exception as e:
        print e
        print cur._last_executed
        status = 2
        return HttpResponse(e.message);
    cur.close()
    conn.close()
    return HttpResponse(status);

def get_recent_articles(request):
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800;")
    cur.execute("select link, title, updated from rss_content where tag='Manual' order by updated desc")
    rows = cur.fetchall()
    cur.close()
    conn.close()
    data = json.dumps(rows, cls=DateTimeEncoder)
    return HttpResponse(data)

def check_article(request):
    link = request.GET['link']
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800;")
    cur.execute("select link, title, content, updated from rss_content where link=%s or original_link=%s", (link, link))
    rows = cur.fetchall()
    cur.close()
    conn.close()
    data = json.dumps(rows, cls=DateTimeEncoder)
    return HttpResponse(data)


def upload_artist_csv(request):
    f = request.FILES['csvArtistCsv']
    gallery = request.POST['gallery']
    gallery_name = request.POST['gallery_name']
    status = 0
    content = f.readlines()
    status = 0
    if len(content[0].split(",")) != 1:
        status = 1
        context = {'gallery':gallery,
                   'gallery_name':gallery_name,
                   'status' :status,
        }
        return render(request, 'art/file_upload.html', context)
    if len(content) <= 1:
        status = 3
        context = {'gallery':gallery,
                   'gallery_name':gallery_name,
                   'status' :status,
        }
        return render(request, 'art/file_upload.html', context)
    array = []
    for line in content:
        if len(line.split(",")) != 1:
            status = 4
            context = {'gallery':gallery,
                    'gallery_name':gallery_name,
                    'status' :status,
            }
            return render(request, 'art/file_upload.html', context)
        '''
        if len(line.split(" ")) <= 1:
            status = 5
            context = {'gallery':gallery,
                    'gallery_name':gallery_name,
                    'status' :status,
            }
            return render(request, 'art/file_upload.html', context)
        '''
        line = line.replace('"', "")
        array.append(" ".join(line.strip().split()))
    data = json.dumps(array)
    context = {'artists':array,
               'gallery':gallery,
               'gallery_name':gallery_name,
               'status' :status,
    }
    return render(request, 'art/file_upload.html', context)

def add_mapping(request):
    source_gallery = request.GET['source_gallery']
    dest_gallery = request.GET['dest_gallery']
    source_mapping = get_gallery_mapping(source_gallery)
    dest_mapping = get_gallery_mapping(dest_gallery)
    status = 0
    if source_mapping != -1:
        status = 1
        return HttpResponse(status)
    if dest_mapping != -1:
        status = 1
        return HttpResponse(status)
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True)
    conn.autocommit(True)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800;")
    try:
        cur.execute("UPDATE gallery SET mapping = %s where id = %s", (dest_gallery, source_gallery))
    except Exception as e:
        print e
        print cur._last_executed
        status = 2
        return HttpResponse(status)
    cur.close()
    conn.close()
    #return HttpResponse(str(status) + " " + str(source_mapping) + " " + str(dest_mapping))
    return HttpResponse(status)

def remove_mapping(request):
    gallery = request.GET['gallery']
    mapping = get_gallery_mapping(gallery)
    status = 0
    if mapping == -1:
        status = 1
        return HttpResponse(status)
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True)
    conn.autocommit(True)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800;")
    try:
        cur.execute("UPDATE gallery SET mapping = -1 where id = %s", (gallery, ))
    except Exception as e:
        print e
        print cur._last_executed
        status = 2
        return HttpResponse(status)
    cur.close()
    conn.close()
    #return HttpResponse(str(status) + " " + str(source_mapping) + " " + str(dest_mapping))
    return HttpResponse(status)


def changelog(request):
    """
    Loads add feed html page which redirects to tinyrss to add feed

    **Template:**

    :template: 'art/add_feed.html'

    """
    return render(request, 'art/changelog.html')
