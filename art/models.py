# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class Artists(models.Model):
    artistid = models.IntegerField(db_column='artistId', primary_key=True)  # Field name made lowercase.
    artistname = models.CharField(db_column='artistName', max_length=200, blank=True, null=True)  # Field name made lowercase.
    artistpopularityall = models.IntegerField(db_column='artistPopularityAll', blank=True, null=True)  # Field name made lowercase.
    artistpopularityrecent = models.IntegerField(db_column='artistPopularityRecent', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Artists'


class Class(models.Model):
    student = models.CharField(db_column='Student', max_length=255, blank=True, null=True)  # Field name made lowercase.
    course = models.CharField(db_column='Course', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Class'


class Frequents(models.Model):
    bar = models.CharField(max_length=255, blank=True, null=True)
    drinker = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'FREQUENTS'


class Artfacts(models.Model):
    name = models.TextField()
    rank = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'artfacts'


class ArtfactsOld(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    url = models.CharField(max_length=255, blank=True, null=True)
    rank = models.IntegerField(blank=True, null=True)
    change = models.CharField(max_length=50, blank=True, null=True)
    change_direction = models.CharField(max_length=50, blank=True, null=True)
    change_magnitude = models.IntegerField(blank=True, null=True)
    birthyear = models.CharField(db_column='birthYear', max_length=255, blank=True, null=True)  # Field name made lowercase.
    deathyear = models.CharField(db_column='deathYear', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'artfacts_old'


class ArtistCache(models.Model):
    gallery_id = models.IntegerField()
    name = models.TextField()
    type = models.TextField()
    updated = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'artist_cache'


class ArtistScore(models.Model):
    name = models.TextField(primary_key=True)
    score = models.FloatField()
    updated = models.DateTimeField()
    status = models.CharField(max_length=10)

    class Meta:
        managed = False
        db_table = 'artist_score'


class ArtistTop10K(models.Model):
    artistid = models.IntegerField(db_column='artistId', primary_key=True)  # Field name made lowercase.
    artistname = models.CharField(db_column='artistName', max_length=200, blank=True, null=True)  # Field name made lowercase.
    artistpopularityall = models.IntegerField(db_column='artistPopularityAll', blank=True, null=True)  # Field name made lowercase.
    artistpopularityrecent = models.IntegerField(db_column='artistPopularityRecent', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'artist_top10k'


class ArtistTop5K(models.Model):
    artistid = models.IntegerField(db_column='artistId', primary_key=True)  # Field name made lowercase.
    name = models.CharField(max_length=200, blank=True, null=True)
    rank = models.IntegerField(blank=True, null=True)
    artistpopularityrecent = models.IntegerField(db_column='artistPopularityRecent', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'artist_top5k'


class CleanArticles(models.Model):
    article = models.TextField()
    type = models.TextField()

    class Meta:
        managed = False
        db_table = 'clean_articles'


class DailyLocationUpdate(models.Model):
    lat = models.FloatField()
    lng = models.FloatField()
    date = models.DateTimeField()
    daily = models.CharField(max_length=1)
    desc = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'daily_location_update'


class DiscogsArtists(models.Model):
    name = models.TextField()

    class Meta:
        managed = False
        db_table = 'discogs_artists'


class DomainGoogle(models.Model):
    domain = models.TextField()
    wiki = models.BigIntegerField()
    wikiart = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'domain_google'


class DomainRank(models.Model):
    domain = models.TextField()
    rank = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'domain_rank'


class FutureCrawling(models.Model):
    domain = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'future_crawling'


class Gallery(models.Model):
    name = models.TextField()
    place_id = models.TextField(blank=True, null=True)
    address = models.TextField()
    website = models.TextField()
    location = models.TextField()  # This field type is a guess.
    last_updated = models.DateTimeField()
    next_update = models.DateField()
    mapping = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'gallery'


class GoogleSearch(models.Model):
    name = models.TextField()
    count = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'google_search'


class RssArtistIndex(models.Model):
    name = models.CharField(max_length=200)
    link = models.CharField(max_length=767)
    updated = models.DateTimeField()
    domain = models.CharField(max_length=100)
    date = models.DateTimeField()
    title = models.CharField(max_length=500)
    artfacts_rank = models.IntegerField()
    blacklist = models.TextField()  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'rss_artist_index'
        unique_together = (('link', 'name', 'artfacts_rank'),)


class RssArtistIndexMusic(models.Model):
    name = models.CharField(max_length=200)
    link = models.CharField(max_length=767)
    domain = models.CharField(max_length=100)
    date = models.DateTimeField()
    title = models.CharField(max_length=500)
    artfacts_rank = models.IntegerField()
    blacklist = models.TextField()  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'rss_artist_index_music'
        unique_together = (('link', 'name', 'artfacts_rank'),)


class RssContent(models.Model):
    link = models.CharField(primary_key=True, max_length=767)
    original_link = models.CharField(max_length=767)
    domain = models.CharField(max_length=200)
    date = models.DateTimeField()
    original_date = models.DateTimeField()
    title = models.CharField(max_length=1000)
    content = models.TextField()
    blacklist = models.TextField()  # This field type is a guess.
    tag = models.CharField(max_length=10, blank=True, null=True)
    updated = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'rss_content'


class RssContentMusic(models.Model):
    link = models.CharField(primary_key=True, max_length=767)
    original_link = models.CharField(max_length=767)
    domain = models.CharField(max_length=200)
    date = models.DateTimeField()
    original_date = models.DateTimeField()
    title = models.CharField(max_length=1000)
    content = models.TextField()
    blacklist = models.TextField()  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'rss_content_music'


class SvmArt(models.Model):
    link = models.CharField(primary_key=True, max_length=767)
    prediction = models.CharField(max_length=10)
    updated = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'svm_art'


class TestA(models.Model):
    col1 = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'test_a'


class TestB(models.Model):
    col1 = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'test_b'


class Toplists(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=200)
    query = models.CharField(max_length=10000)

    class Meta:
        managed = False
        db_table = 'topLists'


class Top100Artists(models.Model):
    rank = models.IntegerField(blank=True, null=True)
    artist = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'top_100_artists'
