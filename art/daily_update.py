import MySQLdb
from MySQLdb import cursors
from googleplaces import GooglePlaces, types
from factual import Factual
from factual.utils import circle
from time import sleep
import datetime
from random import randrange
from datetime import timedelta
import sys, getopt
import math
from googlesearch import GoogleSearch
import traceback
from collections import OrderedDict
import operator
import datetime as dt
import random
import re
import urllib2
from bs4 import BeautifulSoup
from urlparse import urljoin
from urlparse import urlparse
import pyvirtualdisplay
from selenium import webdriver
from timeout import timeout

GOOGLE_FAILS = 0
FACTUAL_FAILS = 0
debug = False
ua = [
            "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6",
            "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:5.0) Gecko/20100101 Firefox/5.0",
            "Mozilla/5.0 (Windows NT 6.1.1; rv:5.0) Gecko/20100101 Firefox/5.0",
            "Mozilla/5.0 (X11; U; Linux i586; de; rv:5.0) Gecko/20100101 Firefox/5.0",
            "Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.1 (KHTML, like Gecko) Ubuntu/11.04 Chromium/14.0.825.0 Chrome/14.0.825.0 Safari/535.1",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/14.0.824.0 Safari/535.1",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:5.0) Gecko/20100101 Firefox/5.0",
            "Mozilla/5.0 (Macintosh; PPC MacOS X; rv:5.0) Gecko/20110615 Firefox/5.0",
            "Mozilla/5.0 (Windows; U; MSIE 9.0; WIndows NT 9.0; en-US))",
            "Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 5.2; Trident/4.0; Media Center PC 4.0; SLCC1; .NET CLR 3.0.04320)",
            "Mozilla/5.0 (Windows; U; MSIE 7.0; Windows NT 6.0; en-US)",
            "Mozilla/5.0 (compatible; Konqueror/4.5; FreeBSD) KHTML/4.5.4 (like Gecko)",
            "Opera/9.80 (Windows NT 6.1; U; es-ES) Presto/2.9.181 Version/12.00",
            "Opera/9.80 (X11; Linux x86_64; U; fr) Presto/2.9.168 Version/11.50",
            "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_8; de-at) AppleWebKit/533.21.1 (KHTML, like Gecko) Version/5.0.5 Safari/533.21.1",
            "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_7; da-dk) AppleWebKit/533.21.1 (KHTML, like Gecko) Version/5.0.5 Safari/533.21.1",
    ]

def write_error(url, error):
    with open("error_list.csv", "a") as f:
        f.write(url + "," + error + "\n")

def crawl(seeds, pattern, depth=1):
    print "\n\nstarting crawl with pattern: " + pattern
    frontier = seeds
    visited_urls = set()
    final_url = []
    for crawl_url in frontier:
        #print "Crawling:", crawl_url[0]
        visited_urls.add(crawl_url[0])
        try:
            headers = { 'User-Agent' : random.choice(ua)}
            req = urllib2.Request(crawl_url[0], None, headers)
            c = urllib2.urlopen(req)
        except:
            print "Could not access: %s"%(crawl_url,)
            write_error(crawl_url[0], "Could not access website")
            continue
        content_type = c.info().get('Content-Type')
        if not content_type.startswith('text/html'):
            print "Not html"
            continue
        html = c.read()
        #with open("soup.html", "w") as f:
        #    f.write(html)
        soup = BeautifulSoup(html)
        discovered_urls = set()
        links = soup.find_all('a')
        #print links
        links = soup('a')    # Get all anchor tags
        #print "soup"
        #print links
        #print "Found links: "
        #print links
        #print crawl_url
        if crawl_url[1] < depth:
            #print "curr depth: " + str(crawl_url[1])
            #print "links"
            #print links
            for link in links:
                #print "processing: "
                #print link
                if ('href' in dict(link.attrs)):
                    url = urljoin(crawl_url[0], link['href'])
                    inFrontier = False
                    for f in frontier:
                        if url == f[0]:
                            inFrontier = True

                    if (url not in visited_urls
                            and url not in discovered_urls and not inFrontier):
                        domain = urlparse(url).netloc
                        #print "Domain: " + domain
                        for f in frontier:
                            if domain in f[0]:
                                #print "matching url: " + url + " with " + domain
                                patternre = re.compile(pattern)
                                #new_url = url.replace("http://", "")
                                #new_url = new_url.replace("https://", "")
                                if patternre.match(url):
                                    #print url + " matched pattern"
                                    discovered_urls.add(url)
                                    final_url.append(url)
                                #else:
                                    #print url + " didnt match pattern"
            for new_url in discovered_urls:
                frontier.append((new_url, crawl_url[1]+1))
            #frontier += discovered_urls
        #else:
            #break

    #print "-"*20
    return final_url

@timeout(30)
def get_rendered_html(url, btype = 0):
    print "Getting Rendered HTML for url: " + url
    with open("/home/home/Documents/main_website/rendered.txt", "a") as f:
    	f.write(url+"\n")
    #p = subprocess.Popen('pkill -f phantomjs', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    #for line in p.stdout.readlines():
            #print line,
    #retval = p.wait()
    #display = Display(visible=0, size=(800, 600))
    #display.start()
    #chromedriver = '/usr/bin/chromedriver'
    #os.environ["webdriver.chrome.driver"] = chromedriver
    html = ""
    with pyvirtualdisplay.Display(visible=False):
        if btype == 0:
            #binary = FirefoxBinary("/usr/bin/firefox", log_file="firefox.log")
            #firefox_profile = webdriver.FirefoxProfile()
            #firefox_profile.set_preference('permissions.default.stylesheet', 2)
            #firefox_profile.set_preference('permissions.default.image', 2)
            #firefox_profile.set_preference('dom.ipc.plugins.enabled.libflashplayer.so', 'false')
            #browser = webdriver.Firefox(firefox_profile, binary)
        #elif btype == 1:
            #browser = webdriver.Chrome()
        #elif btype == 2:
            browser = webdriver.PhantomJS(service_log_path='/tmp/ghostdriver.log', service_args=["--webdriver-loglevel=ERROR"])
        browser.get(url)
        sleep(10)
        html = browser.page_source
        #with open("test.html", "w") as f:
        #    f.write(html.encode('utf8'))
        browser.close()
        browser.quit()
        #browser.service.process.send_signal(signal.SIGTERM)
    #display.stop()
    if isinstance(html, str):
        print "*"*5 + "string is ascii"
        html=html.replace("&nbsp;", " ");
    elif isinstance(html, unicode):
        print "*"*5 + "string is unicode"
        html=html.replace(u'\xa0', u' ');
    return html
    #print orig_html
    #return browser.page_source

def getArtistLink(html):
    #print html
    #soup = BeautifulSoup(html)
    #html = soup.renderContents()
    #print html

    #with open("alink.html", "w") as f:
        #f.write(html)
    links = None
    try:
        hxs = HtmlXPathSelector(text=html)
        #try:
        links = hxs.select("//a[translate(normalize-space(.), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')='artists']/@href").extract()
        print "Get Artist links: "
        print links
        if links:
            print "returning 1"
            return links
        links = hxs.select("//a[translate(normalize-space(.), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')='artist']/@href").extract()
        print "Get Artist links2: "
        print links
        if links:
            print "returning 2"
            return links
        #links = hxs.select("//*[contains(@href,'artists') and not(contains(@href, '@'))]/@href").extract()
        #print "Get Artist link3"
        #print links
        #if links:
            #print "returning 3"
            #return links
        links = hxs.select("//*[contains(@href,'directory') and not(contains(@href, '@'))]/@href").extract()
        print "Get Artist link4"
        print links
        if links:
            print "returning 4"
            return links


    except:
        try:
            links = hxs.select("//*[contains(@href,'artists')]/@href").extract()
            print "Get Artist link5"
            print links
            return links
        except:
            return None
    #except:
        #link = hxs.select("//a[text()='Artist']/@href").extract()[0]
        #print link
        #if "artist" in link.lower():
            #return link
    return links

def get_html(url):
    #browser = webdriver.Firefox()
    #browser.get(url)
    #return browser.page_source
    print "Getting HTML"
    headers = { 'User-Agent' : random.choice(ua)}
    req = urllib2.Request(url, None, headers)
    try:
        c = urllib2.urlopen(req)
        response = c.read()
    except:
        print "Could not access", url
        return None
    html = response
    #html = html.replace("<span>", "")
    #html = html.replace("</span>", "")
    if isinstance(html, str):
        print "*"*5 + "string is ascii"
        try:
            html=html.replace("&nbsp;", " ")
            html = unicode(html, errors='ignore')
        except Exception as e:
            print e
            pass
    elif isinstance(html, unicode):
        print "*"*5 + "string is unicode"
        try:
            html=html.replace(u'\xa0', u' ');
        except Exception as e:
            print e
            pass
    return html

def random_date():
    """
    This function will return a random datetime between two datetime
    objects.
    """
    start = (datetime.datetime.now())
    end = (datetime.datetime.now() + datetime.timedelta(days=365.25/2))
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = randrange(int_delta)
    return start + timedelta(seconds=random_second)

def update_next(place_id, url):
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True)
    conn.autocommit(True)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800;")
    random_next_update = random_date()
    cur.execute("update gallery set next_update = %s where place_id = %s and website = %s", (random_next_update, place_id, url))

def gallery_get(place_id, url, new=True):
    print "gallery get called"
    update_next(place_id, url)
    original_url = url
    url = url
    html = get_html(url)
    pattern = "((http|https):\/\/)?[a-zA-Z0-9!#$%&'()*+,\-.:;<=>?_`~\/]+\/[a-zA-Z0-9!#$%&'()*+,\-.:;<=>?_`~]*artist[a-zA-Z0-9!#$%&'()*+,\-.:;<=>?^_`]*(\/)?$"
    final_urls = crawl([(url, 0)],pattern)
    if len(final_urls) == 0:
        print "Trying without http"
        pattern = "(?<=href=\")[a-zA-Z0-9!#$%&'()*+,\-.:;<=>?_`~\/]*artist[a-zA-Z0-9!#$%&'()*+,\-.:;<=>?^_`]*(\/)?"
        #pattern = "href=\"[a-zA-Z0-9!#$%&'()*+,\-.:;<=>?_`~\/]*artist[a-zA-Z0-9!#$%&'()*+,\-.:;<=>?^_`]*(\/)?\""
        #pattern = "[a-zA-Z0-9!#$%&'()*+,\-.:;<=>?_`~\/]*artist[a-zA-Z0-9!#$%&'()*+,\-.:;<=>?^_`]*(\/)?$"
        final_urls = crawl([(url, 0)],pattern)
        if len(final_urls) == 0:
            print "Trying without http and directory"
            pattern = "[a-zA-Z0-9!#$%&'()*+,\-.:;<=>?_`~\/]*directory[a-zA-Z0-9!#$%&'()*+,\-.:;<=>?^_`]*(\/)?$"
            final_urls = crawl([(url, 0)],pattern)

        #print html
        print "FOUND URLS:"
        print final_urls
        if len(final_urls) == 0:
            #try rendered html
            #html = get_rendered_html(url)
            print "No URLS Found, trying artist url"
            artistLink = getArtistLink(html)
            print artistLink
            if artistLink:
                print "Artist Link Found"
                print artistLink
                for aLink in artistLink:
                    print "link in alink: " + aLink
                    if "http://" not in aLink:
                        aLink = urljoin(url, aLink)
                        print url
                        print aLink
                    final_urls.append(aLink)
            else:
                print "Artist Link Not Found"
                html = get_rendered_html(url)
                artistLink = getArtistLink(html)
                if artistLink:
                    for aLink in artistLink:
                        if "http://" not in aLink:
                            aLink = urljoin(url, aLink)
                            print aLink
                        final_urls.append(aLink)
                else:
                    write_error(url, "No artist link found")
                    return "No Artist Link Found"
        else:
            artistLink = getArtistLink(html)
            if artistLink:
                print "Artist Link Found 1"
                for aLink in artistLink:
                    if "http://" not in aLink:
                        aLink = urljoin(url, aLink)
                    if aLink not in final_urls:
                        final_urls.append(aLink)
        print "Using Following URLS"
        print final_urls
        for i in final_urls:
            if "http" not in i:
                final_urls.remove(i)
        good = []
        bad = []
        for url in final_urls:
            print "original_url"
            print url
            print original_url
            print urljoin(original_url, "#")
            if url == original_url or url == original_url + "#":
                continue
            print "current url"
            print final_urls
            print url
            html = get_html(url)
            if not html:
                print "No HTML Found"
                html = get_rendered_html(url)
                if not html:
                    print "No Rendered HTML Found"
                    continue
            #print html
            #with open("test.html", "w") as f:
                #f.write(html)
            (terms, bad_terms) = parse_terms(html)
            try:
                if len(terms) <= 2:
                    html = get_rendered_html(url)
                    (terms, bad_terms) = parse_terms(html)
                    #with open("tmp.html", "w") as f:
                        #f.write(html)
                    print "firefox"
                    print terms
                    if len(terms) <= 2:
                        terms = parse_wix(html)
                        print "wix"
                        print terms
                        if not terms:
                            continue
            except:
                continue
            print "Got Following Terms:"
            print terms
            temp = run_xpath(terms,html)
            #try:
            bad += bad_terms
            if temp:
                (g, b) = temp
                if g == [] and b == []:
                    html = get_rendered_html(url)
                    temp = run_xpath(terms,html)

                if g:
                    good+=g
                if b:
                    bad+=b
	#return HttpResponse("Bad: [" + ", ".join(bad) + "]<br/><br/>" + "Good: [" + ", ".join(good) + "]")
        print "URLS"
        print url
        print '-' * 20
        print final_urls
        print '-' * 20
        #score = []
        #for g in good:
        if len(good) == 0:
            write_error(url, "No Names Found")
        bad = list(set(bad))
        good = list(set(good))
        for b in bad:
            if b in good:
                bad.remove(b)
        scores = get_artist_score(good)
        result = []
        for k in scores:
            temp = [k, scores[k], get_artist_status(None, k)]
            result.append(temp)

        if new:
            print "saving artists"
            print good
            print bad
            print scores
            save_artists(original_url, good, bad, scores)
        return HttpResponse(json.dumps(result))
        #return HttpResponse("" + "<br>".join(['%s::%s' % (key, value) for (key, value) in scores.items()]) + "")
    #return httpresponse(url)

def get_artists_from_gallery(gallery_id):
    print "getting gallery with id: " + str(gallery_id)
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
    #cur = conn.cursor()
    #cur.execute("select name from artist_cache where gallery_id = %s and type = 'Good'", (gallery_id, ))
    #names = cur.fetchall()
    #for name in names:
    #    get_score(None, name[0])
    #cur.close()
    cur = conn.cursor()
    cur.execute("select c.name, score from artist_cache c join artist_score s on c.name = s.name where gallery_id = %s and type = 'Good' order by score desc", (gallery_id, ))
    names= []
    rows = cur.fetchall()
    scores = {}
    for row in rows:
        scores[row[0]] = row[1]
    cur.close()
    scores = OrderedDict(sorted(scores.items(), key=operator.itemgetter(1), reverse=True))
    #scores = get_artist_score(names)
    conn.close()
    return scores

def check_gallery(place_id, url):
    print "check_gallery"
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True)
    conn.autocommit(True)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800;")
    cur.execute("select next_update from gallery where place_id = %s and website = %s", (place_id, url))
    rows = cur.fetchall()
    next_update = rows[0][0]
    print "next_update!!!!: %s"%(next_update)
    if next_update > dt.date.today():
        return False
    else:
        return True

def get_artist_status(name):
    if not name:
        name = request.GET['name']
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
    cur = conn.cursor()
    cur.execute("select count(*) from artfacts where name = %s", (name, ))
    name_count = cur.fetchall()[0][0]
    cur.close()
    conn.close()
    if name_count > 0:
        return "Known"
    else:
        return "Unknown"

def get_gallery_names(gallery_id, place_id, url, force=False):
    if gallery_id == -1:
        print "gallery_id -1"
        if force or check_gallery(place_id, url):
            return gallery_get(place_id, url, True)
        else:
            return []
    else:
        #print "getting artists from gallery: " + name
        artists = get_artists_from_gallery(gallery_id)
        if not artists:
            if force or check_gallery(place_id, url):
                return gallery_get(place_id, url, True)
            else:
                return []
            #return gallery_get(request, True)
        else:
            result = []
            for k in artists:
                temp = [k, artists[k], get_artist_status(k)]
                result.append(temp)
            return result
    return []

def save_new_gallery(name, place_id, address, website, lat, lon):
    print name, place_id, address, website, lat, lon
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True)
    conn.autocommit(True)
    cur = conn.cursor()
    cur.execute("SET NET_WRITE_TIMEOUT = 28800;")
    cur.execute("select id from gallery where place_id = %s or (X(location) = %s and Y(location) = %s)", (place_id, lat, lon))
    rows = cur.fetchall()
    print rows
    if len(rows) == 0:
        try:
            cur.execute("INSERT INTO gallery (name, place_id, address, website, location) VALUES (%s, %s, %s, %s, GeomFromText(%s))", (name, place_id, address, website, "POINT(" + str(lat) + " " +  str(lon) + ")"))
            conn.commit()
        except Exception as e:
            print e
            print cur._last_executed
            raise

        cur.close()
        conn.close()
        print "gallery not found"
        return -1
        #row = cur.fetchall()[0][0]
    else:
        cur.execute("select * from artist_cache where gallery_id = %s", (rows[0][0], ))
        num_artists = cur.fetchall()
        cur.close()
        conn.close()
        if len(num_artists) == 0:
            print "no artists found in gallery"
            return -1
        else:
            print "found gallery with artists"
            return rows[0][0]

def update(lat, lng):
    global GOOGLE_FAILS, FACTUAL_FAILS
    print "updating: %s, %s"%(lat, lng)
    DEFAULT_RADIUS = 5
    API_KEY = "AIzaSyCPfugPVQjJ40XVL_m3s7V-rMOQ1HpHZCw"
    API_KEY2 = "AIzaSyBfH2wBi5LR6ZBZjGiQjhHf3F12BpPoAP0"
    google_locations = []
    if GOOGLE_FAILS <= 10:
        try:
            print "try google"
            google_places = GooglePlaces(API_KEY)
            next_page_token = None
            for i in range(0, 3):
                sleep(2)
                if i == 0:
                    print "query with lat: %s lng: %s"%(lat, lng)
                    query_result = google_places.nearby_search(
                        lat_lng={'lat': lat, 'lng': lng},
                        name = "gallery",
                        types = types.TYPE_ART_GALLERY,)
                elif next_page_token:
                    print "query with next_page %s"%(next_page_token)
                    query_result = google_places.nearby_search(
                            next_page_token=next_page_token)
                next_page_token = query_result.next_page_key
                print "next_page_token: %s"%(next_page_token)
                galleries = []
                for place in query_result.places:
                    temp = []
                    place.get_details()
                    print place.geo_location
                    if place.website == None:
                        continue
                    try:
                        gallery_id = save_new_gallery(place.name,place.place_id, place.formatted_address, place.website, place.geo_location['lat'], place.geo_location['lng'])
                    except:
                        continue
                    temp.append(gallery_id)
                    temp.append(place.name)
                    temp.append(place.website)
                    temp.append(place.geo_location['lat'])
                    temp.append(place.geo_location['lng'])
                    temp.append(place.place_id)
                    google_locations.append(temp)
        except Exception as e:
            print "exception google"
            print e
            traceback.print_exc()
            try:
                google_places = GooglePlaces(API_KEY2)
                next_page_token = None
                google_locations = []
                for i in range(0, 3):
                    sleep(2)
                    if i == 0:
                        print "query with lat: %s lng: %s"%(lat, lng)
                        query_result = google_places.nearby_search(
                            lat_lng={'lat': lat, 'lng': lng},
                            name = "gallery",
                            types = types.TYPE_ART_GALLERY,)
                    elif next_page_token:
                        print "query with next_page %s"%(next_page_token)
                        query_result = google_places.nearby_search(
                                next_page_token=next_page_token)
                    next_page_token = query_result.next_page_key
                    print "next_page_token: %s"%(next_page_token)
                    galleries = []
                    for place in query_result.places:
                        temp = []
                        place.get_details()
                        print place.geo_location
                        if place.website == None:
                            continue
                        try:
                            gallery_id = save_new_gallery(place.name,place.place_id, place.formatted_address, place.website, place.geo_location['lat'], place.geo_location['lng'])
                        except:
                            continue
                        temp.append(gallery_id)
                        temp.append(place.name)
                        temp.append(place.website)
                        temp.append(place.geo_location['lat'])
                        temp.append(place.geo_location['lng'])
                        temp.append(place.place_id)
                        google_locations.append(temp)
            except Exception as e:
                print "exception google 2"
                print traceback.print_exc()
                GOOGLE_FAILS += 1


    API_KEY = '3leY3SOw4EnuDcGf6K1VidS6ncdaAZg2t5RxATPl'
    API_SECRET = 'tU6JxyJA3UOvfvAOFxczMJiRcz1eQXSKk3URsVHi'
    f = Factual(API_KEY, API_SECRET)
    places = f.table('places')
    factual_locations = []
    for i in range(0, 10):
        #data = places.filters({"$and":[{"country":{"$eq":"US"}},{"region":{"$eq":"NY"}},{"locality":{"$eq":"NEW YORK"}},{"category_labels":{"$includes":'["SOCIAL","ARTS","ART DEALERS AND GALLERIES"]'}}]}).offset(i*50).limit(50).data()
        data = places.filters({"category_labels":{"$includes":'["SOCIAL","ARTS","ART DEALERS AND GALLERIES"]'}}).geo(circle(lat, lng, 5000)).offset(i*50).limit(50).data()
        for place in data:
            if 'website' not in place:
                continue
            temp = []
            try:
                gallery_id = save_new_gallery(place['name'],place['factual_id'],place['address'] + ", " +  place['locality'] + ", " + place['region'] + "," +  place['postcode'] + "," + place['country'], place['website'], place['latitude'], place['longitude'])
            except:
                continue
            temp.append(gallery_id)
            temp.append(place['name'])
            temp.append(place['website'])
            temp.append(place['latitude'])
            temp.append(place['longitude'])
            temp.append(place['factual_id'])
            factual_locations.append(temp)
    for location in google_locations:
        get_gallery_names(location[0], location[5], location[2])
    for location in factual_locations:
        get_gallery_names(location[0], location[5], location[2])

def get_article_score(name, title, content, link):
    try:
        if name.lower() in title.lower():
            return 10

        temp = name.split()
        if temp[0].lower() in link.lower() and temp[1].lower() in link.lower():
            return 10
        if name.lower()in content.lower():
            temp = name.split()
            fcount = content.lower().count(temp[0])
            lcount = content.lower().count(temp[1])
            if fcount > 5 or lcount > 5:
                return 10
            if fcount > 4 or lcount > 4:
                return 8
            if fcount > 3 or lcount > 3:
                return 6
            if fcount > 2 or lcount > 2:
                return 4
            if fcount > 1 or lcount > 1:
                return 2
            return 1
        else:
            #print title
            return 1
    except Exception as e:
            return 1

def get_recency_score(date):
    if date == None:
        return .1
    today = datetime.date.today()
    months = {1:1, 3:.9, 6:.7, 12:.5, 24:.4, 36:.2}
    for month in months:
        margin = datetime.timedelta(month*30)
        if date.date() >= (today - margin):
            return months[month]
    print date
    return .1

def get_artist_status(request, name=None):
    if not name:
        name = request.GET['name']
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
    cur = conn.cursor()
    cur.execute("select count(*) from artfacts where name = %s", (name, ))
    name_count = cur.fetchall()[0][0]
    cur.close()
    conn.close()
    if name_count > 0:
        return "Known"
    else:
        return "Unknown"

def get_final_article_score(name, title, content, domain, date, link):
    try:
        conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
        cur = conn.cursor()
        cur.execute("SELECT score FROM artist_score where name=%s", (name, ))
        score = cur.fetchall()
        if len(score) != 0:
            exist = True
        cur.close()
    except Exception as e:
        return
    #get domain score
    cur = conn.cursor()
    cur.execute("select wikiart from domain_google where domain = %s", (domain, ))
    dscore = cur.fetchall()
    if len(dscore) == 0:
        try:
            count = GoogleSearch("\"%s\" site:wikipedia.org"%(domain), use_proxy=False).count()
            time.sleep(5)
            countart = GoogleSearch("\"%s\",art site:wikipedia.org"%(domain), use_proxy=False).count()
            cur = conn.cursor()
            print "domain: %s, wiki:%s, wikiart:%s"%(domain, count, countart)
            try:
                cur.execute("INSERT INTO domain_google (domain, wiki, wikiart) VALUES (%s, %s, %s)", (domain, count, countart))
                conn.commit()
            except:
                print cur._last_executed
                raise
            cur.close()
            time.sleep(5)
            cur = conn.cursor()
            cur.execute("select wikiart from domain_google where domain = %s", (domain, ))
            dscore = cur.fetchall()[0][0]
        except:
            dscore = 10
    else:
        dscore = dscore[0][0]
    if dscore < 10:
        dscore = 10
    cur.close()
    #print "dscore: %s"%(dscore)
    artscore = get_article_score(name, title, content, link)#article[1], article[2])
    recency = get_recency_score(date)
    ascore = artscore*recency
    return (math.log(dscore, 10)*ascore,"art: %s, rec: %s, dom: %s"%(str(artscore), str(recency), str(math.log(dscore, 10))))

def get_score(name):
    if not name:
        return
    exist = False
    try:
        conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
        cur = conn.cursor()
        cur.execute("SELECT score FROM artist_score where name=%s", (name, ))
        score = cur.fetchall()
        if len(score) != 0:
            exist = True
        cur.close()
    except Exception as e:
        return
    try:
        cur = conn.cursor()
        cur.execute("select domain, title, content, original_date, a.link from rss_content a join svm_art b on a.link = b.link where prediction = 'Art' and match(content, title) against('\"" + name + "\"' in boolean mode) and blacklist = 0")
        articles = cur.fetchall()
        cur.close()
    except:
        return
    article_score = []
    domainscore = []
    average = 0L
    for article in articles:
        #get domain score
        cur = conn.cursor()
        cur.execute("select wikiart from domain_google where domain = %s", (article[0], ))
        dscore = cur.fetchall()
        if len(dscore) == 0:
            try:
                count = GoogleSearch("\"%s\" site:wikipedia.org"%(domain[0]), use_proxy=False).count()
                time.sleep(5)
                countart = GoogleSearch("\"%s\",art site:wikipedia.org"%(domain[0]), use_proxy=False).count()
                cur = conn.cursor()
                print "domain: %s, wiki:%s, wikiart:%s"%(domain[0], count, countart)
                try:
                    cur.execute("INSERT INTO domain_google (domain, wiki, wikiart) VALUES (%s, %s, %s)", (domain[0], count, countart))
                    conn.commit()
                except:
                    print cur._last_executed
                    raise
                cur.close()
                time.sleep(5)
                cur = conn.cursor()
                cur.execute("select wikiart from domain_google where domain = %s", (article[0], ))
                dscore = cur.fetchall()[0][0]
            except:
                dscore = 1
        else:
            dscore = dscore[0][0]
        if dscore < 10:
            dscore = 10
        domainscore.append(dscore)
        cur.close()
        #print "dscore: %s"%(dscore)
        ascore = get_article_score(name, article[1], article[2], article[4])
        ascore = ascore*get_recency_score(article[3])
        if debug:
            print "a: %s ascore: %s"%(article[1], ascore)
            print "d: %s logd: %s"%(dscore, math.log(dscore, 10))
        try:
            article_score.append((dscore,math.log(dscore, 10), ascore))
            average = average+(math.log(dscore, 10)*ascore)
        except Exception as e:
            print e
            article_score.append((dscore,0L, ascore))

#    if len(article_score) != 0:
#        average = average/len(article_score)*math.log(len(articles))
    print "Artist: %s, Score: %s"%(name, average)
    cur = conn.cursor()
    status = get_artist_status(None, name)
    if not exist:
        cur.execute("INSERT INTO artist_score (name, score, status) values (%s, %s, %s)", (name, average, status))
    else:
        cur.execute("UPDATE artist_score SET score = %s, status=%s WHERE name =%s", (average, status, name))
    conn.commit()
    cur.close()
    conn.close()


#update today's visited locations with few default locations
#TODO update galleries
if len(sys.argv) == 1 or sys.argv[1] == "gallery":
    try:
        conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
        cur = conn.cursor()
        cur.execute("SELECT distinct lat, lng, daily FROM daily_location_update;")
        locations = cur.fetchall()
        cur.close()
        conn.close()
        for location in locations:
            lat = location[0]
            lng = location[1]
            try:
                update(lat, lng)
                if location[2] == '0':
                    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
                    conn.autocommit(True)
                    cur = conn.cursor()
                    cur.execute("DELETE FROM daily_location_update WHERE abs(`lat` - %s) <= 1e-3 AND abs(`lng` - %s) <= 1e-3;", (lat, lng))
                    cur.close()
                    conn.close()
            except Exception as e:
                print "FAILED: %s"%(e)
                traceback.print_exc()
                continue
    except Exception as e:
        print e


#TODO update artist scores
if len(sys.argv)== 1 or sys.argv[1] == "artist":
    artist = None
    if len(sys.argv) == 3:
        artist = sys.argv[2]
        debug = True
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
    cur = conn.cursor()
    if artist:
        cur.execute("SELECT distinct name FROM artist_score where name='" + artist + "';")
    else:
        cur.execute("SELECT distinct name FROM artist_cache where type='Good' union select name from artist_score union select name from artfacts;")
    names = cur.fetchall()
    cur.close()
    conn.close()
    print names
    for name in names:
        get_score(name[0])

if sys.argv[1] == "gallery_all":
    conn = MySQLdb.connect(user='spjps2009', passwd='paintingisfun', db='mainDB', host='localhost', charset="utf8", use_unicode=True, cursorclass = cursors.Cursor)
    cur = conn.cursor()
    cur.execute("select g1.id, g1.place_id, g1.website from gallery g1 where g1.website is not null and g1.id not in (select id from gallery g join artist_cache a on g.id = a.gallery_id where website is not null group by g.id) group by g1.website")
    locations = cur.fetchall()
    cur.close()
    conn.close()
    for location in locations:
        id = location[0]
        place_id = location[1]
        website = location[2]
        try:
            get_gallery_names(id, place_id, website, True)
        except Exception as e:
            print "FAILED: %s"%(e)
            traceback.print_exc()
            continue


