from django.shortcuts import render
from django.http import HttpResponse
from art.models import RssArtistIndex,RssContent
from django.core import serializers
from django.db import connections
import urllib2
import json
import datetime


# Create your views here.
def index(request):
    #template = loader.get_template('art/index.html')
    nextoffset = 0
    previousoffset = 0
    try:
        nextoffset = int(request.GET['offset'])
    except Exception as e:
        nextoffset = 0


    link_list = RssArtistIndex.objects.raw("SELECT id, a.name, a.link, a.domain, a.date, a.title, count(*) as numArtist FROM rss_artist_index_music a where a.blacklist = 0 group by a.link order by a.date desc, a.name limit 20 offset %s", [nextoffset])
    trending = RssArtistIndex.objects.raw("SELECT id, name, count(*)-1 as total from rss_artist_index_music where date >= (SELECT a.date FROM rss_artist_index_music a group by DATE(a.date) order by a.date desc limit 1) group by name having count(*) > 1 order by count(*) desc limit 20")
    date = RssArtistIndex.objects.raw("SELECT id, DATE(a.date) FROM rss_artist_index_music a group by DATE(a.date) order by a.date desc limit 1")
    cursor = connections['data'].cursor()
    cursor.execute("SELECT count(*) from (SELECT id, a.name, a.link, a.domain, a.date, a.title, count(*) as numArtist FROM rss_artist_index_music a where a.blacklist = 0 group by a.link order by a.date desc, a.name) temp")
    row = cursor.fetchone()
    print nextoffset
    print row[0]
    if nextoffset+20 >= row[0]:
        if nextoffset == 0:
            previousoffset = nextoffset
            nextoffset = nextoffset
        else:
            previousoffset = nextoffset - 20
            nextoffset = nextoffset
    else:
        if nextoffset == 0:
            previousoffset = 0
        else:
            previousoffset = nextoffset - 20
        nextoffset = nextoffset + 20
    context = {'link_list':link_list,
                'trending':trending,
                'date' :date,
                'offset':nextoffset,
                'previousoffset':previousoffset,
        }
    return render(request, 'music/index.html', context)

def stats(request):
    artist_total = RssArtistIndex.objects.raw("SELECT id, count(distinct name) as total FROM rss_artist_index_music a where a.blacklist = 0")
    artist_rankings = RssArtistIndex.objects.raw("SELECT id, name, artfacts_rank, count(*) as score FROM rss_artist_index_music a where a.blacklist = 0 group by a.name")
    artist_rankings2 = RssArtistIndex.objects.raw("SELECT id, artfacts_rank, count(*) as total FROM rss_artist_index_music a where a.blacklist = 0 and artfacts_rank > 1 group by artfacts_rank")
    start_date = (datetime.date.today() - datetime.timedelta(1*365/12)).strftime('%Y-%m-%d %H:%M:%S')
    feed_total = RssArtistIndex.objects.raw("SELECT id, count(distinct link) as total FROM rss_artist_index_music a where a.blacklist = 0")
    live_feed = RssArtistIndex.objects.raw("SELECT id, DATE(date) as date, domain, count(*) as total FROM rss_artist_index_music a where a.blacklist = 0 and date > %s group by DATE(date), domain having total < 500",[start_date] )
    artists_per_day = RssArtistIndex.objects.raw("SELECT id, DATE(date) as date, count(distinct name) as total FROM rss_artist_index_music a where a.blacklist = 0 and date > %s GROUP BY DATE(date) order by date desc",[start_date] )
    articles_per_day = RssArtistIndex.objects.raw("SELECT id, DATE(date) as date, count(distinct link) as total FROM rss_artist_index_music a where a.blacklist = 0 and date > %s GROUP BY DATE(date) order by date desc",[start_date] )
    article_percentage = RssContent.objects.raw("select link, a.domain, count(*) * 100 / (select count(*) from rss_content_music) as percent from rss_content_music a group by domain order by percent desc;");

    context = {
                'live_feed':live_feed,
                'feed_total':feed_total,
                'artist_total':artist_total,
                'artist_rankings':artist_rankings,
                'artist_rankings2':artist_rankings2,
                'artists_per_day' : artists_per_day,
                'articles_per_day': articles_per_day,
                'article_percentage': article_percentage,
                }
    return render(request, 'music/stats.html', context)

def clean(request):
    clean = RssContent.objects.raw("SELECT distinct a.link, a.domain, a.date, a.title, a.content from rss_content_music a join rss_artist_index_music b on a.link = b.link where name not in (select name from artfacts) order by date desc;")
    context = {
                'link_list' : clean
                }
    return render(request, 'music/clean.html', context)

def trash(request):
    artist_list = RssArtistIndex.objects.raw("SELECT id, a.link, a.domain, a.date, a.title FROM rss_artist_index_music a where a.blacklist = 1 order by a.date desc limit 150")
    context = {'link_list':artist_list,
                }
    return render(request, 'music/trash.html', context)

def getScore(request, name):
    artist_score = RssArtistIndex.objects.filter(name=name).count()
    return HttpResponse(artist_score)

def getDomain(request, name):
    artist_domain = RssArtistIndex.objects.filter(name=name).values('domain').distinct()
    data = json.dumps(list(artist_domain))
    return HttpResponse(data)

def getLink(request, name):
    artist_all = RssArtistIndex.objects.raw("SELECT id, a.link, a.domain, b.date, a.title FROM rss_artist_index_music a join rss_content_music b on a.link=b.link where name='" + name + "' and a.blacklist = 0 group by a.link order by b.date desc")
    data = serializers.serialize("json", artist_all)
    return HttpResponse(data)

def getTrash(request, name):
    artist_all = RssArtistIndex.objects.raw("SELECT id, a.link, a.domain, b.date, a.title FROM rss_artist_index_music a join rss_content_music b on a.link=b.link where name=%s and a.blacklist = 1 group by a.link order by b.date desc", [name])
    data = serializers.serialize("json", artist_all)
    return HttpResponse(data)

def getLatest(request):
    artist_all = RssArtistIndex.objects.raw("SELECT id, a.link, a.domain, b.date, a.title FROM rss_artist_index_music a join rss_content_music b on a.link=b.link where blacklist = 0 group by a.link order by b.date desc limit 300")
    data = serializers.serialize("json", artist_all)
    return HttpResponse(data)

def getArtist(request):
    url = request.GET['url']
    url = urllib2.unquote(url.decode("utf8"))
    name = urllib2.unquote(request.GET['name'].decode("utf-8"))
    artist_all = RssArtistIndex.objects.raw("SELECT id, name FROM rss_artist_index_music where link = %s and name <> %s order by name desc", [url, name])
    data = serializers.serialize("json", artist_all, fields=("name"))
    return HttpResponse(data)

def search(request):
    name = request.GET['q']
    artist_list = RssArtistIndex.objects.raw("SELECT id, a.name, max(date) as date FROM rss_artist_index_music a where name RLIKE %s group by name order by count(*) desc limit 30", ['[[:<:]]' + name + '[[:>:]]'])
    context = {'link_list':artist_list,}
        #print >>sys.stderr, p
    return render(request, 'music/search.html', context)

def removeLinkArtist(request):
    url = request.GET['url']
    name = urllib2.unquote(request.GET['name'].decode("utf-8"))
    url = urllib2.unquote(url.decode("utf8"))
    cursor = connections['data'].cursor()
    cursor.execute("UPDATE rss_artist_index_music set blacklist=1 where link = %s and name = %s", [url,name])
    return HttpResponse(url + " " + name)

def restoreLinkArtist(request):
    url = request.GET['url']
    name = urllib2.unquote(request.GET['name'].decode("utf-8"))
    url = urllib2.unquote(url.decode("utf8"))
    cursor = connections['data'].cursor()
    cursor.execute("UPDATE rss_artist_index_music set blacklist=0 where link = %s and name = %s", [url,name])
    return HttpResponse(url)

def removeLink(request):
    url = request.GET['url']
    url = urllib2.unquote(url.decode("utf8"))
    cursor = connections['data'].cursor()
    cursor.execute("UPDATE rss_artist_index_music set blacklist=1 where link = %s", [url])
    cursor.execute("UPDATE rss_content_music set blacklist=1 where link = %s", [url])
    return HttpResponse(url)

def restoreLink(request):
    url = request.GET['url']
    url = urllib2.unquote(url.decode("utf8"))
    cursor = connections['data'].cursor()
    cursor.execute("UPDATE rss_artist_index_music set blacklist=0 where link = %s", [url])
    cursor.execute("UPDATE rss_content_music set blacklist=0 where link = %s", [url])
    return HttpResponse(url)

def getMore(request):
    url = request.GET['url']
    url = urllib2.unquote(url.decode("utf8"))
    cursor = connections['data'].cursor()
    cursor.execute("Select * from rss_content_music where original_link = %s", [url])
    row = cursor.fetchone()
    artists = []
    cursor.execute("select artistName from artist_top10k")
    rows = cursor.fetchall()
    for r in rows:
        name = r[0].split(" ")
        if len(name) <= 1:
            continue
        if len(r[0]) < 4:
            continue
        if len(name[0]) <= 2 or len(name[1]) <= 2:
            continue
        if r[0] in row[6]:
            artists.append(r[0])

    artists_json = json.dumps(artists)
    return HttpResponse(artists_json)

