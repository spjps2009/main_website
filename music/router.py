from django.conf import settings

class MusicRouter(object):
    """
    A router to control all database operations on models in the
    art application.
    """
    def db_for_read(self, model, **hints):
        """
        Attempts to read art models go to data.
        """
        if model._meta.app_label == 'music':
            return 'data'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write art models go to data.
        """
        if model._meta.app_label == 'music':
            return 'data'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the art app is involved.
        """
        if obj1._meta.app_label == 'music' or \
           obj2._meta.app_label == 'music':
           return True
        return None

    def allow_migrate(self, db, model):
        """
        Make sure the art app only appears in the 'data'
        database.
        """
        if db == 'data':
            return model._meta.app_label == 'music'
        elif model._meta.app_label == 'music':
            return False
        return None

