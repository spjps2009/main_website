from django.conf.urls import url
from music import views

urlpatterns = [#patterns('music.views',
        url(r'^$', views.index, name="index"),
        url(r'^trash/$', views.trash, name="trash"),
        url(r'^stats/$', views.stats, name="stats"),
        url(r'^clean/$', views.clean, name="clean"),
        url(r'^removeLinkArtist/$', views.removeLinkArtist, name="removeLinkArtist"),
        url(r'^restoreLinkArtist/$', views.restoreLinkArtist, name="restoreLinkArtist"),
        url(r'^removeLink/$', views.removeLink, name="removeLink"),
        url(r'^restoreLink/$', views.restoreLink, name="restoreLink"),
        url(r'^getScore/(?P<name>[\w|\W]+)/$', views.getScore, name="getScore"),
        url(r'^getDomain/(?P<name>[\w|\W]+)/$', views.getDomain, name="getDomain"),
        url(r'^getLink/(?P<name>[\w|\W]+)/$', views.getLink, name="getLink"),
        url(r'^getTrash/(?P<name>[\w|\W]+)/$', views.getTrash, name="getTrash"),
        url(r'^getLatest/$', views.getLatest, name="getLatest"),
        url(r'^getMore/$', views.getMore, name="getMore"),
        url(r'^getArtist/$', views.getArtist, name="getArtist"),
        url(r'^search/$', views.search, name="search"),
]#)
